---
title: 'An overview of the Paged Media × PrePostPrint workshop'
langen: true
titleEn: 'An overview of the Paged Media × PrePostPrint workshop'
langfr: false
langen: true
titleFr: "An overview of the Paged Media × PrePostPrint workshop"
date: 2018-12-11T18:00:00+02:00
draft: false
externalSite: "pagedmedia.org"
externalURL: "https://web.archive.org/web/20190506055446/https://www.pagedmedia.org/an-overview-of-the-paged-media-x-prepostprint-workshop/"
authors: ["Julie Blanc"]
tags: ["paged.js", "prepostprint", "workshop", "CSS", "CSS print"]
---



A few days ago, from Wednesday 28 to Friday 30 November, a group of people interested in automated composition with HTML and CSS met for a series of workshops about  “Paged Media × PrePostPrint”.

Paged Media is a community with the goal of advocating for the use of CSS Paged Media. We are creating open source tools to support using print CSS with current browsers. In recent months we have been working on developing [Paged.js](https://www.pagedjs.org/), an open source JavaScript library that paginates content in browsers.

[PrePostPrint](https://web.archive.org/web/20190506055617/https://www.prepostprint.org/doku.php/fr/introduction) is an informal group of people (mostly in France and Belgium) that is interested in alternative free publishing workflows, particularly in the creation of hybrid and printed publications with web technologies. This network and this energy resource seemed to us to be the ideal place for first public experiments with paged.js.


Consequently we organised 3 workshops! One in Brussels, and 2 in Paris. Earlier we also held one in San Francisco and we are planning more for next year, including a workshop in New York around March.

The photos below are from the workshop in Paris on Nov 30. You can find more photos from the three days [on the Cabbage Tree Labs website](https://web.archive.org/web/20190506055446/https://photos.cabbagetreelabs.org/).

{{< img src="images/9J1A7871+.jpg" >}}

{{< img src="images/9J1A7362+.JPG" >}}

The workshops were open to all and led by the team at the heart of  paged.js development (Fred Chasen, Julie Blanc and Julien Taquet). Unfortunately founder [Adam Hyde](https://web.archive.org/web/20190506055446/https://www.adamhyde.net/) was not able to make it but we hope he will for next time! These three days was an opportunity to discuss, to improve paged.js, to discover everyone’s expectations, to listen to needs, and to experience a number of happy accidents.

Each day had its own flavor and we were able to meet great people with different backgrounds: publishers, designers, writers, developers (sometimes all this in one person). Everyone brought us their insights, ideas, questions and solutions with great enthusiasm and energy.

Each day began with a long presentation of paged.js followed by  open discussions including:

- A presentation of the context: what is paged media and how paged.js appears.
- A technical explanation of the script and other libraries or commands used for the script (slides [here](https://web.archive.org/web/20190506055446/https://docs.google.com/presentation/d/1SoIaOo_GtBZJ1cdJW3chj1ebF4p4YvHxIU5KY2ffk54/edit#slide=id.g4848baf641_0_113)).
- A designer look at how paged.js handles pagination and what designers can do with the tool with the description of the CSS specifications used in paged.js and a book typesetting demonstration.
- A look at the state of paged.js today, what it can do, what’s missing, and what initial projects are being built on top of it.
- And finally, how paged.js works under the hood and how to override styles, and how to add other JavaScript libraries to tweak the output (this part was addressed to those who were most technically comfortable).

{{< img src="images/9J1A7826+.JPG" >}}

After installing the script on the participants’ machines, the afternoon was spent in practice, in groups or individually. Then, just before closing the workshop, some people presented the results of their experiments. The results were pretty good.

Some have imagined how to integrate paged.js into their tools and workflows. Yihui Xie (RStudio) and Romain Lesur had already been working on integrating paged.js into the tool [Pagedown R package](https://github.com/rstudio/pagedown) for a few weeks. Louis Éveillard, from L’atelier des chercheurs, plans to integrate paged.js into the print output of the [do.doc tool](https://latelier-des-chercheurs.fr/outils/dodoc). Some publishers also have successfully tested the script to create stylesheets for their collections. Vincent W.J. Van Gerven Oei wrote [a beautiful post](https://web.archive.org/web/20190506055446/https://editoria.pub/2018/11/29/an-editoria-community-perspective-from-the-paged-js-workshop/) (thank you!) about publishers using paged.js and Arthur Violy [recreated some pages](https://web.archive.org/web/20190506055446/https://ppp-x-pagedjs.nouveaudocument.fr/) from the first book of the publishing house Nouveau document.


{{< img src="images/arthur-violy.png" caption="Arthur Violy recreate the stylesheet of the first book of Nouveau document with paged.js" class="border">}}

Others, especially graphic designers and artists, experimented with paged.js using crazy CSS properties (variables fonts, gradients, rotations, etc.) and artistic generative script. Paged.js has sometimes been broken, often for the better, but we also noticed that the script was resilient enough to support all these crazy ideas! We took the opportunity to fix some bugs and develop new features such as this [sidenote script](https://gitlab.pagedmedia.org/tools/experiments/tree/master/sidenotes). Thomas Parisot also helped us to find new, easier, ways to deploy paged.js for workshops and improve Paged.js-based PDF generation [from Asciidoc documents](https://github.com/Mogztter/asciidoctor-pdf.js/pull/22).


{{< img src="images/Martin-Deknudt.png" caption="Martin Deknudt created a printed book with scroll effect on images" class="border">}}

{{< img src="images/leo-martin.png" caption="Léo Martin use paged.js and personal script to create a generative book with some emoji" class="border">}}

<figure>
    <img src="/blog/2018-12-11_paged-media-x-prepostprint/images/lucile-haute.gif">
    <figcaption>Lucile Haute created a printed version of <a target="_blank" href="https://lucilehaute.fr/">her online portfolio</a> with paged.js</figcaption>
</figure>

These workshops allowed us to see the whole spectrum of paged.js in use cases to move the project forward. This is the first time that paged.js has been tested by so many people and we could see that a new community was born. We would like to thank all participants warmly and are full of enthusiasm for future workshops. It was a success for us.

*The first workshop was held in Brussels in the Open Source Publishing office and the second and third workshops were held in Paris in the EnsadLab office. Thanks to them and many thanks also to the Shuttleworth Foundation who supported these workshops.*

You can find more from the workshops [on the Cabbage Tree Labs website](https://photos.cabbagetreelabs.org/) and a selection [on my server](https://workshops.julie-blanc.fr/2018-pagedmedia-x-ppp/).

{{< img src="images/WEB_WORKSHOP_PAGES_JS_WORLD_TRADE_CENTER_28_11_18_BRUSSELS_BELGIUM_D819930.jpg">}}

{{< img src="images/WEB_WORKSHOP_PAGES_JS_WORLD_TRADE_CENTER_28_11_18_BRUSSELS_BELGIUM_D810108.jpg">}}

{{< img src="images/WEB_WORKSHOP_PAGES_JS_WORLD_TRADE_CENTER_28_11_18_BRUSSELS_BELGIUM_DSC4519.jpg">}}








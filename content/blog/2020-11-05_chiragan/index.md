---
title: "Une chaîne de publication collaborative et multisupport pour le musée Saint-Raymond"
langen: false
titleEn: ""
langfr: true
titleFr: "Une chaîne de publication collaborative et multisupport pour le musée Saint-Raymond"
date: 2020-11-05T12:00:00+02:00
draft: false
authors: ["Julie Blanc"]
tags: ["Chiragan", "publication", "multisupport", "single source publishing"]
linkedProject: "villa-chiragan"
---





Voilà bientôt quatre ans que je m'intéresse aux systèmes de publication utilisant les technologies du web et proposant une diffusion multisupport. J'ai eu l'occasion d'en parler quelques fois, dans des conférences, des publications ou des discussions plus informelles. Au début très focalisée sur les aspects techniques, j'ai pu peu à peu m'interroger sur la manière dont les pratiques collaboratives sont transformée dans ces systèmes de publications ou quelles implications cela supposait sur les pratiques de design graphique et éditorial. Beaucoup de discours, sans jamais avoir l'occasion de mettre les mains à la pâte. Alors, quand [Antoine Fauchié](https://www.quaternum.net/) m'a proposé, il y a deux ans déjà, de se joindre à lui pour travailler sur un [catalogue numérique](https://villachiragan.saintraymond.toulouse.fr/) et imprimé pour le musée Saint-Raymond de Toulouse, je n'ai pas hésité. 

Dans ce billet, je reviens sur ce projet dont l'originalité repose sur sa chaîne de publication où l’ensemble technique sollicité est celui du web et de ses standards – à la fois pour l'écriture, l'édition, la composition et la diffusion des catalogues. Dans la première partie, je présente le système technique crée spécifiquement pour l’équipe du musée et basé sur un ensemble d’outils *open-source* assemblés. Dans la seconde partie, je montre comment l'utilisation de [Git](https://git-scm.com/), un outil incontournable dans les pratiques de programmation, a permis de mettre en place de nouvelles modalités de collaborations entre les différents acteurs du projet. 





## Une chaîne de publication sur-mesure basée sur les technologies du web



Fin 2018, Christelle Moliné, responsable des ressources documentaire du [musée Saint-Raymond](https://saintraymond.toulouse.fr/), fait appel à Antoine et moi pour créer un catalogue numérique et un catalogue imprimé afin de valoriser une de ses collections, un ensemble de plus de 700 sculptures romaines provenant (du lieu-dit) de la villa Chiragan, à Martres-Tolosane à environ 60km de Toulouse. L'équipe du musée est particulièrement intéressée par la conception d'un système de publication facilitant cette diffusion multisupport. La demande est de produire un système ressemblant à celui mis en place  par l'équipe du Getty Museum de Los Angeles et qu'Antoine avait étudié.[^getty]

Le projet se constitue autour d'une équipe de quatre personnes : Antoine Fauchié dirige la conception de la chaîne de publication, Christelle Moliné assure la coordination éditoriale, Pascal Capus écrit les textes des catalogues et je prends en charge la conception graphique et le développement web. Le projet a duré près de deux ans (entre octobre 2018 et juin 2020); le catalogue numérique est aujourd'hui [en ligne](https://villachiragan.saintraymond.toulouse.fr) – depuis septembre 2019 – et la catalogue imprimé est en cours d'impression.


{{< img class="border" src="images/responsive-chiragan-pages.jpg" >}}






### Les technologies du web

Avant d'aller plus loin, j'aimerais faire un point sur le web et le fonctionnement de ses technologies. Le web est constitué de trois langages principaux qui sont des standards ouverts: HTML (*HyperText Markup Language*), CSS (*Cascading Style Sheets*) et JavaScript. Le principe de séparation technique entre le contenu (structuration sémantique par l’HTML) et sa mise en forme (présentation visuelle par le CSS) est au cœur du web. Les feuilles de style CSS sont ainsi un ensemble d’indications destinée à décrire des paramètres d’affichage pour chaque type d’élément HTML [^css-zen-garden]. Elles permettent des mises en formes diversifiées des textes en fonction des préférences utilisateurs, de la taille de l'écran ou de supports de sortie (au premier rang desquels la distinction entre écrans et pages imprimées[^pdf]).

Si la maîtrise du rendu des pages web était complexe il y a encore dix ans, les navigateurs sont aujourd’hui devenus de puissants moteurs de rendu qui peuvent être considérés comme des outils de composition. Les évolutions récentes de CSS permettent en effet des mises en forme de plus en plus poussées, et avec des scripts comme [Paged.js](https://www.pagedjs.org/), de nouvelles perspective ont été aussi ouvertes pour la mise en page imprimée. 

Pour être assez rapide et parce que j'aurais d'autres occasion d'en parler, Paged.js est un polyfill permettant d'utiliser le langage CSS pour concevoir des livres imprimés depuis les navigateurs web. Un polyfill est un palliatif logiciel implémentant une fonctionnalité non encore nativement disponible dans celui-ci. Ici, Paged.js prend en charge l'implémentation dans les navigateurs web de syntaxes CSS dédiées aux médias paginés et à l'impression. 

[^css-zen-garden]: Le site *CSS Zen Garden*, créé en 2003 par Dave Shea, se veut la démonstration de la possibilité de modifier librement le rendu affiché d'une même page web, uniquement grâce à CSS et sans aucune modification de son code HTML.

[^pdf]: Une option dans les boîtes de dialogues d’impression des navigateurs web permet aussi de générer un PDF, permettant de prévoir une impression au-delà des imprimantes de bureautique.



### La chaîne de publication

Dans son billet [*Une chaîne de publication inspirée du web*](https://www.quaternum.net/2017/03/13/une-chaine-de-publication-inspiree-du-web/) (2017), Antoine Fauchié démontre que « le web influence aujourd’hui le domaine du livre jusque dans ses outils de production : une chaîne de publication peut être élaborée en s’éloignant des modèles existants classiques » et se baser sur des pratiques plus en phase avec le caractère multiforme des publications multisupport. Avec les technologies du web, reposant sur des standards ouverts, il est possible de construire ou accommoder des briques logiciels modulaires et interopérables pouvant ensuite être assemblées pour former le noyau technique d’une chaîne éditoriale. Ainsi, le système technique peut être adapté selon la diversité et la variabilité des situations de travail réel présentes dans une chaîne éditoriale spécifique. C'est sur ces principes qu'a été conçue et développée la chaîne de publication pour l'équipe du musée Saint-Raymond.

La chaîne de publication repose sur le principe du [*single source publishing*](https://en.wikipedia.org/wiki/Single-source_publishing), une méthode de gestion de contenu qui permet de produire plusieurs sorties (site web, livre numérique, livre imprimé, etc.) à partir d'un même contenu. <span style="hyphens: none">Les fichiers sources pivots sont ici produits en Markdown, un langage de balisage très léger, lisible autant pour l'humain que pour la machine et qu'il est possible d'apprendre en moins d'une heure. </span>Ces fichiers  sont visibles et modifiables indépendamment de la technologies utilisée pour concevoir les différentes sorties, ils peuvent aussi être écrits depuis n'importe quel logiciel.

Les fichiers Markdown sont ensuite transformés en fichiers HTML structurés et organisés : plusieurs fichiers HTML sont produits pour le site web et un fichier HTML est produit pour le catalogue imprimé, mobilisant un contenu réduit. C'est un générateur de site statique qui se charge de cette conversion en se basant sur des *templates* qui indiquent la structuration globale des pages HTML à produire. Il existe énormément de générateurs de sites statiques, nous avons choisi [Jekyll](https://jekyllrb.com/), l'un des plus connus, selon les préférences d'Antoine. Pour mettre en forme les catalogues, j'ai conçu une feuille de style CSS pour le site web et une autre feuille de style pour le catalogue imprimé. Cette dernière feuille de style a été couplée à Paged.js et le PDF du catalogue imprimé a été généré dans un navigateur web (Chrome). La figure ci-dessous représente de manière schématique ce fonctionnement en *single source publishing* où deux sorties (le catalogue numérique et le catalogue imprimé) ont été produites à partir des mêmes fichiers Markdown.


{{< img src="images/single-source-publishing.png" caption="Représentation simplifiée du fonctionnement en *single source publishing* de la chaîne éditoriale">}}


Pour gérer l'évolution du projet, soit les différentes versions du fichiers et leur partage entre nous, nous avons utilisé le système de versionnement git et l'éditeur Forestry.io Je vais y revenir plus précisément dans le point suivant de ce billet. Un système de gestions des références bibliographiques, Zotero, a aussi été ajouté à la chaîne éditoriale pour faciliter l'appel des références. 

Les différents éléments (forestry.io, Zotero, GitLab, Jekyll, paged.js) sont «&nbsp;branchés&nbsp;» entre eux pour former la chaîne éditoriale. Cela est possible car ils sont constitués de langages ouverts et interopérables. Si l'un de ces éléments devient défaillant ou ne répond plus à un besoin, il est toujours possible de le remplacer par un autre de même type, ce qui rend la chaîne éditoriale résiliente à travers le temps. 

La figure ci-dessous présente un schéma plus détaillé du fonctionnement technique de la chaîne de publication : 

{{< img src="images/shema-workflow-MSR.png">}}




## Git au centre des pratiques de production collaboratives



Dans une chaîne de publication plus classique, nous observons souvent des pratiques en silo, où chaque métier utilise son propre logiciel avec une compatibilité souvent difficile, voir impossible. Alors que les auteurs, éditeurs et correcteurs partagent depuis bien longtemps un outil de travail commun (les logiciels de traitement de texte), les outils de mise en page ne laissent, eux, que peu de place à la collaboration.  Généralement, le travail du ou de la designer graphique commence lorsque le texte est déjà plus ou moins finalisé, relu et corrigé par les éditeurs. Une fois qu'il a terminé une première version de la mise en page, il l'envoie à l'éditeur sous forme de PDF. Commence alors un long processus de collaboration en ping-pong basés sur l'échange de PDF annotés où l'éditeur demande corrections orthotypographiques et édition du contenu. En effet, dès qu’un contenu est intégré à un logiciel de publication assistée par ordinateur comme InDesign, les personnes capables de le modifier sont celles qui ont accès au logiciel – qui le possède — et qui savent le manipuler — qui ont la connaissance technique nécessaire. C'est donc bien souvent les designers graphiques qui deviennent seuls responsables du contenu à partir du moment où un livre commence à être mis en page pour son impression. 

Ce processus de collaboration, lent et fastidieux se retrouve transformé avec l'utilisation des technologies du web. En effet, l'utilisation d'outils basés sur les technologies du web dans une chaîne de publication offre un avantage non négligeable : celui de bénéficier de l'écosystème de travail collaboratif développé depuis plus de vingt ans par des dizaines de milliers de développeurs. 

Pour la dernière partie de ce billet, j'aimerais me concentrer sur la manière dont l'utilisation de Git a transformé les modalités d'actions collaboratives entre les acteurs et actrices de ce projet éditorial.



### Fonctionnement de Git

Git est un logiciel de gestion de versions qui utilise des dépôts de fichiers décentralisés. Les logiciels de gestion de versions sont utilisés principalement par les développeurs pour suivre l’évolution d’un code source et travailler à plusieurs sur un même ensemble de fichiers. Ces logiciels sont capables de suivre l’évolution d’un fichier ligne de code par ligne de code, de conserver les anciennes versions et revenir en arrière en cas de problème. Ils facilitent aussi la transmission des modifications d'un fichier : en se connectant à Git, les participants ont toujours la dernière version du fichier et dès qu'ils modifient celui-ci, le changement est enregistré. 

Le fonctionnement de Git repose sur des *commits*, qui, pour le dire assez simplement, correspondent chacun à une version du code source au cours du temps ou un jalon d'avancement du travail (c'est beaucoup plus complexe en réalité). Chaque participant du projet va ainsi travailler sur le code source en local de son côté. Il ou elle peut, à tout moment, choisir d'enregistrer les modifications et ajouts effectués dans les fichiers. Cet enregistrement devient alors un *commit* (une sorte de livraison du travail à un moment choisi). Grâce au fonctionnement complexe du versionnage, si deux personnes modifient un même fichier en même temps, Git est capable de comparer automatiquement ces deux versions et de les fusionner en une nouvelle version contenant les modifications effectuées par les deux participants. [^merge-conflit]  Tous les *commits* sont enregistrés dans le système Git et il est possible de visualiser les changements au cours du temps, de ramener le projet à un état précédent ou encore de supprimer un commit du projet sans affecter les commits effectués après lui. Ces *commits* contiennent par ailleurs tout un ensemble de métadonnées qui facilitent le travail de collaboration: ils sont à minima défini par un titre, une date, l'auteur et les métadonnées associées indiquant quelles modifications et ajouts ont été fait sur quel fichier.

[^merge-conflit]: Si une même ligne de code a été transformée différemment par les deux participants, il y a alors un conflit de version qui est signalé et pour poursuivre le travail, il faut alors choisir entre les deux possibilités. 

{{< img class="border" src="images/commits-repo.png" caption="Extrait de la liste des *commits* du projet">}}



### Utilisation de Git dans la chaîne éditoriale: collaborer de manière synchrone

Nous avons pu utiliser Git dans ce projet car tous les participants (auteur, éditrice, développeur et designer) manipulent le même type de fichiers textuels constituant le code source des catalogues. Ainsi, Git est utilisé à la fois pour construire le système technique (fichiers HTML et CSS permettant de construire le site) mais aussi pour enregistrer le contenu du site et ses modifications (fichiers sources en Markdown). Tous les participants du projet peuvent alors travailler de manière synchronisées ou désynchronisées sur les mêmes fichiers en accédant facilement aux modifications des autres participants. Là où dans une chaîne de publication classique, la collaboration se fait à travers des échanges de mails et de fichiers différenciés, ici, les participants travaillent à partir du même code source, qui est toujours tenu à jour. 

J'ai montré plus haut comment tout changement effectué par l'auteur ou l'éditrice dans le fichier Markdown est automatiquement répercuté dans le site web et le catalogue imprimé grâce au principe du *single source publishing*. Je pouvais donc avancer sur la mise en forme des catalogues, sans me préoccuper de l'avancement des textes. Dès que ceux-ci étaient modifiés, par le système de Git, le changement était automatiquement répercuté dans mes fichiers. Grâce au principe du versionnage, il ne peut y avoir de perte d'informations car les *commit* sont disponibles pour savoir quand, pourquoi et quels changements ont été effectués.

Cette disponibilité constante permet de ne pas attendre qu'un participant avance sur son travail pour avancer sur le sien. Je n'ai pas attendu de recevoir les textes finalisés de l'équipe muséale pour commencer à développer le catalogue numérique. Les gabarits des pages pouvaient être développés en parallèle de l'écriture du contenu. Au fur et à mesure que ce dernier était créé par l'équipe muséale, les pages web se créaint automatiquement. En retour, je pouvais affiner les gabarits en fonction du contenu de plus en plus précis. De même, lorsque j'ai commencé à travailler sur la version imprimée du catalogue, l'équipe muséale pouvait accéder à tout moment à l'évolution du fichier source et afficher le rendu paginé. S'ils trouvaient une correction à faire, ils pouvaient l'effectuer directement dans le code source sans que je m'en préoccupe.


Les temporalités de la collaboration sont ainsi profondément différentes par rapport à une chaîne éditoriale classique, car tous les participants travaillent techniquement sur les mêmes fichiers, de manière plus ou moins synchrone. La figure ci-dessous illustre le nombre de *commits* créé par les participants en fonction de l'avancée du projet. Elle permet de mettre en évidence que les quatres participants ont travaillé de manière synchrone sur les mêmes fichiers sources, et ce tout au long du projet. 

{{< img class="border" src="images/commits-stat.jpg" caption="Nombre de *commits* par participants en fonction de la durée du projet">}}



### Une interface graphique pour l'équipe muséale

Si la chaîne de publication est commune à tous les participants, nous ne travaillons pas tous dans les mêmes environnements. Antoine et moi travaillons les fichiers sources à partir d'éditeurs de textes (Visual studio code pour moi) et interagissons avec Git en ligne de commande. Ces logiciels sont trop éloignés des habitudes de l'équipe muséale qui travaille d'ordinaire principalement avec des logiciels à interface graphique tels que Microsoft Word. Afin de faciliter le travail d'écriture et d'édition de l'équipe muséale, Antoine a ajouté une interface graphique à la chaîne d'éditoriale, disponible depuis un site web. [Forestry.io](https://forestry.io/) permet d'afficher les fichiers Markdown de manière graphique et d'interagir avec eux de manière visuelle: boutons pour définir un titre ou ajouter une image, champs de saisies séparées pour les quelques métadonnées, sélection et organisation des fichiers dans une liste ordonnées, etc. À chaque fois que les modifications d'un fichier étaient sauvegardée, un *commit* était automatiquement ajouté au dépôt de fichier de Git. De plus l'interface proposait un lien permettant d'afficher une prévisualisation du site web avec les modifications apportées. Ainsi, l'équipe muséale n'interagit avec la chaîne technique qu'à travers une interface graphique.


{{< img class="border" src="images/forestry.png" caption="Interface de Forestry.io">}}




&nbsp;



Ce billet a été l'occasion de montrer qu'utiliser les technologies et méthodes du web dans les pratiques éditoriales ouvre, par l'intermédiaire d'outils comme Git, de nouvelles modalités de collaboration, et ce, même dans des projets avec une équipe relativement réduite. Le système technique assemblé pour l'occasion se veut modulaire, léger et repose sur des briques *open source*. Nous sommes loin des systèmes techniques prêts à l'usage, embarquant souvent une myriade de fonctionnalités à peine utilisées et reposant sur des *templates* normatifs destiné à habiller n'importe quel type de contenu. 

J'aimerais terminer ce billet par une réflexion adressée au designers graphiques et aux designers web. Il est possible de défendre des pratiques adaptées à chaque projet et réfléchies en cohérence avec ce qu'une équipe souhaite transmettre. Pour le musée Saint-Raymond, les gabarits des pages web et la publication imprimée ont ainsi été conçu sur mesure [^print], en suivant l'avancée de la rédaction du contenu et en discussion avec l'équipe. Il me semble que cela n'aurait pas été possible sans un minimum de capacité de compréhension du système technique et l'utilisation directe des technologies du web pour concevoir le design graphique et interactif. J'aimerais donc appuyer que c'est là un endroit où ils peuvent insérer leurs compétences afin de proposer des publications multisupport dont la conception est en cohérence avec les contenus. 


[^getty]: Quire est la chaîne de publication développée par Getty Publications, la documentation est en ligne : [https://gettypubs.github.io/quire/](https://gettypubs.github.io/quire/).
[^print]: J'espère trouver le temps de faire un billet sur le design graphique de la version imprimée pour montrer l'utilisation de Paged.js. 


&nbsp;


Quelques liens: 

- le catalogue numérique: https://villachiragan.saintraymond.toulouse.fr/
- le catalogue imprimé généré avec Paged.js: https://villachiragan.saintraymond.toulouse.fr/impression
- le code source du projet (catalogue, contenus et système de publication): https://gitlab.com/musee-saint-raymond/villa-chiragan/
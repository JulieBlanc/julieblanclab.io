---
title: "Un hackathon Paged.js à l'EnsadLab"
langen: true
titleEn: "A paged.js hackathon at EnsadLab"
langfr: true
langen: true
titleFr: "Un hackathon Paged.js à l'EnsadLab"
date: 2021-04-16T12:00:00+02:00
draft: false
externalSite: 'pagedjs.org'
externalURL: 'https://www.pagedjs.org/posts/2021-03-hackathon/'
authors: ["Julie Blanc"]
tags: ["paged.js", "CSS", "CSS print", "workshop"]
id: 'hackathon'
---

{{< img src="images/9J1A5482_retouch.jpg" >}}
{{< img src="images/9J1A5333_retouch.jpg" >}}

Les 1<sup>er</sup> et 2 mars 2021, un petit groupe de designers graphiques s'est réuni à Paris pour un hackathon consacré à Paged.js. L'idée était de réunir des designers graphiques ayant déjà une expérience de Paged.js afin de discuter de leur travail de mise en page et de composition et d'imaginer ensemble de nouvelles possibilités. Le hackathon, organisé par Julie Blanc et Lucile, a accueilli Julien Taquet, Manu Vazquez, Louis Éveillard, Sarah Garcin, Robin de Mourat et Quentin Juhel.

Malgré le contexte sanitaire peu évident, nous avons pu nous réunir à l'EnsadLab et avons eu la chance de pouvoir passer deux journées ensemble.

L'idée était d'explorer le fonctionnement de Paged.js, de travailler sur des scripts pour améliorer les possibilités de l'outil et de discuter les changements qu'implique l'utilisation du CSS pour l'impression autant en termes de mise en page et composition qu'en termes de nouvelles façons de travailler pour les designers graphiques.

Le hackathon a débuté par un tour de table des projets conçus par les participant·es. Lors de la première matinée, nous avons donc exploré les différentes améliorations spécifiques qu'ils·elles ont pu faire de Paged.js  et nous avons discuté des difficultés rencontrées dans la mise en forme des contenus ou dans l’organisation travail avec leurs collègues (auteurs, éditeurs, imprimeurs, etc.) lorsque l'utilisation de différents outils basés sur les technologies du web et Paged.js étaient impliqués. À partir de là, nous avons pu commencer à travailler sur différentes idées de script qui sont apparues au fil de nos échanges.

De manière globale, nous avons discuté et travaillé à partir des [*handlers* et des *hooks*](https://pagedjs.org/documentation/10-handlers-hooks-and-custom-javascript/). Le code source de paged.js est organisé de manière à faciliter l'addition de fonctionnalités à travers des *handlers* – sorte de module à programmer pouvant gérer un type d'évènement. Les *hooks* – pouvant être traduit par « prises » ou « crochets » – sont des sortes de points d'arrêt spécifiques dans l'exécution du script offrant la possibilité d'ajouter son propre code à différents moments de rendu et de pagination du document; par exemple, avant ou après que le CSS soit transformé, avant la pagination du document, avant ou après la fragmentation d'une page spécifique, avant ou après l'addition d'élément à une page, etc.

{{< img src="images/9J1A5203_retouch.jpg" >}}
{{< img src="images/9J1A5476_retouch.jpg" >}}

Tous les codes sources de notre production sont disponibles dans [un dossier dédié de l'instance gitlab hébergeant Paged.js](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021). Attention toutefois, les scripts et outils résultant de ce hackathon ne sont pas directement réutilisables. Certains d'entre eux peuvent être adaptés pour vos projets si vous êtes confortable avec l'utilisation de Paged.js; d'autres nécessitent encore un important travail de développement.  Dans cet article, nous vous proposons un résumé des discussions qui ont été mené et du travail qui a été fait pendant des deux jours.



## Documentation

Sarah a travaillé autour de la ligne de base et a écrit [un début de documentation](https://gitlab.pagedmedia.org/julientaq/hackathon-mars-2021/blob/master/baseline-experiment_sarah/baseline-documentation.md) (en français) issue directement de nos discussions: comment utiliser des variables pour que le contenu soit aligné à une grille de lignes de base, pourquoi utiliser une mesure en pixel plutôt qu'en point, comment faire en sorte de garder une trace visuelle de la grille, etc. Elle a développé [quelques scripts](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/blob/master/baseline-experiment_sarah/baseline-documentation.md) pour aligner automatiquement les éléments (textes et images) à la ligne de base grâce à l'utilisation de la propriété [`offsetTop`](https://developer.mozilla.org/fr/docs/Web/API/HTMLElement/offsetTop). Sarah est même allée plus loin dans l'expérimentation, en proposant un script permettant de positionner les éléments de manière aléatoire dans la page tout en les gardant alignées sur la grille.

{{< img src="images/9J1A5322_retouch.jpg" >}}
{{< img class="border" src="images/baseline.png" >}}

Manu a profité du hackathon pour documenter les scripts disponibles dans le repo [*experiments*](https://gitlab.coko.foundation/pagedjs/experiments) du gitlab de Paged.js. Ce repo contient divers scripts expérimentaux testés et conçus au cours de ces dernières années. Certains d'entre eux sont encore en cours de développement et peuvent être utilisés pour vos projets avec quelques adaptations, mais personne n'avait encore pris le temps de les documenter. Sur la base de son expérience avec BookSprints, il travaille également avec Julie sur un [script](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/delete-margin-content_manu) permettant de supprimer le contenu des boîtes de marge (*margin boxes*) lorsqu'un élément apparaît sur la page.



## Canevas HTML pour les images et pour dessiner



Suite à un récit très inspirant de Sarah à propos des problèmes qu'elle a rencontrés chez des imprimeurs offset avec l'utilisation du CSS `mix-blend-mode` sur certaines images, Robin et Julien ont eu l'idée d'utiliser des canevas HTML [pour recréer l'image et lui appliquer les effets désirés](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/blend-modes). Au-delà du CSS `mix-blend-mode`, l'utilisation des canvas HTML pour les images pourrait permettre une meilleure optimisation des images pour l'impression mais c'est un point que nous n'avons pas fini de creuser.

{{< img src="images/9J1A5636_retouch.jpg" >}}


Louis a aussi travaillé avec les canevas HTLM. Il a conçu [un système](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/canvas_embedding-louis) pour injecter du code JavaScript extérieur et dessiner dans le canevas ou télécharger des API. N'importe quel script peut être ajouté à la volée grâce à l'attribut `data-script`ajouté au canevas; toutefois, la taille du canevas doit être donnée à l'avance pour éviter des problèmes de débordement de page.



## Notes flottantes (*float notes*)

Robin et Julie ont travaillé sur un vieux script qui permet de créer des [notes flottantes](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/notes-float), soit des zones accueillant les notes du texte et pouvant se positionner où l'on veut sur la page. Le script, commencé il y a plus de deux ans, a été amélioré et optimisé car l'ancienne version présentait de sérieux problèmes de fragmentation du contenu textuel lorsque les notes étaient déplacées dans des zones flottantes. L'idée était de pouvoir disposer d'une première implémentation d'une partie des [spécifications pour les notes](https://github.com/w3c/css-print/issues/3) proposée par l'équipe de Paged.js. Ce sont les premières bases mais il y a encore beaucoup de travail à faire pour obtenir un résultat solide (soyez donc prudent·e si vous voulez utiliser le script).

{{< img src="images/9J1A5473_retouch.jpg" >}}
{{< img class="border" src="images/float-notes.png" >}}





## Imposition

Quentin, amoureux inconditionnel de fanzines, fait beaucoup de workshop CSS Print auprès d'étudiant·es. Dans la grande majorité des cas, ils·elles doivent imprimer leurs productions sur les machines dont ils·elles disposent et ont donc besoin d'un moyen d'imposer les pages après que Paged.js ait fini le rendu. Avec l'aide de Julien, il a développé [un script](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/imposition_quentin_juhel) offrant tout ce qu'il faut pour l'imposition: re-organiser la disposition des pages, les réunir en planches ou encore ajouter automatiquement les pages manquantes si leur total n'est pas un multiple de 4. Le script est une première proposition qui doit continuer à être développer pour créer des impositions plus complexes en cahier. Toute l'idée est basée sur un script conçu par Julien Bidoret.

{{< img src="images/9J1A5506_retouch.jpg" >}}


## Grilles de mise en page automatisée

Louis a utilisé [Potpack](https://mapbox.github.io/potpack/), une bibliothèque JavaScript pour arranger des éléments de tailles variables de manière compacte dans un conteneur donné. Il a ainsi fait en sorte de disposer des images sur une page en fonction de leur nombre et de leur taille. Son script, [disponible sur le repo gitlab](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/auto_layout_images-louis), est un bon exemple de la façon dont les algorithmes de mise en page automatique de grilles (par exemple [Masonry](https://masonry.desandro.com/), [Packery](https://packery.metafizzy.co/)) peuvent être facilement ajoutés à Paged.js. L'idée serait de commencer à préparer des modules JavaScript additionnels à Paged.js et directement utilisables avec l'ajout de quelques classes dans le HTML. Cela pourrait être utilisé par des personnes qui ne sont pas des développeurs ou par des designers graphistes qui ne codent pas.

{{< img src="images/9J1A5002_retouch.jpg" >}}



## Céer un lien entre le contenu et le rendu paginé

Durant la discussion de la première matinée du hackathon, Sarah et Julie ont raconté la manière dont la dernière étape de composition des livres – tout ce qui touche aux corrections et la gestion de la microtypographie et des drapeaux – pouvait être très chronophage. La désynchronisation de l'écriture (sur le code source HTML) et du rendu (dans le navigateur web) peut en effet rendre ce travail fastidieux dans certains cas.

{{< img class="border" src="images/paged-editor.png" >}}

Louis a alors cherché un moyen de créer un meilleur lien entre les deux. Il a imaginé [une interface](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/paged_editor-louis) s'affichant directement dans le navigateur avec, à gauche, le contenu éditable en un seul flux et à droite, le rendu paginé dans un affichage page par page. Les deux zones sont liées: cliquer sur un élément permet de l’afficher dans les deux zones de manière synchrone. Il est également possible de modifier le contenu à gauche et de le mettre à jour dans le rendu paginé de droite. C'est un très bon début pour imaginer des interfaces plus avancées intégrant Paged.js comme outil.


{{< img src="images/montage-1.png" >}}


Le hackathon est une façon très intéressante de travailler pour trouver rapidement des idées et tester comment les intégrer dans paged.js. Ces deux jours ont été un succès et nous espérons trouver l'occasion de faire d'autres événements.


*Merci à EnsadLab pour le soutien financier et logistique, Lucile Haute pour l'aide à l'organisation, Agathe Charrel pour les photos et tou·tes les participant·es pour ces deux journées merveilleuses.*



{{< img src="images/9J1A5546_retouch.jpg" >}}
{{< img src="images/9J1A5566_retouch.jpg" >}}
{{< img src="images/9J1A5614_retouch.jpg" >}}
{{< img src="images/9J1A5465_retouch.jpg" >}}
{{< img src="images/9J1A5511_retouch.jpg" >}}
{{< img src="images/9J1A5115_retouch.jpg" >}}
{{< img src="images/9J1A5299_retouch.jpg" >}}
{{< img src="images/9J1A5370_retouch.jpg" >}}
{{< img src="images/9J1A5382_retouch.jpg" >}}
{{< img src="images/9J1A5157_retouch.jpg" >}}
{{< img src="images/9J1A5257_retouch.jpg" >}}
{{< img src="images/9J1A5303_retouch.jpg" >}}
{{< img src="images/9J1A5390_retouch.jpg" >}}
{{< img src="images/9J1A5419_retouch.jpg" >}}
{{< img src="images/9J1A5430_retouch.jpg" >}}
{{< img src="images/9J1A5537_retouch.jpg" >}}
{{< img src="images/9J1A5539_retouch.jpg" >}}
{{< img src="images/9J1A5716_retouch.jpg" >}}
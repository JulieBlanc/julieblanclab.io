---
title: "Médialab (1/3): Une décroissance heureuse"
langen: false
titleEn: ""
langfr: true
langen: false
titleFr: "Médialab (1/3): Une décroissance heureuse"
date: 2020-03-25T12:00:00+02:00
draft: false
authors: ["Julie Blanc", "Benjamin Gremillon"]
tags: ["Médialab", "CSS", "site web", "design graphique"]
linkedProject: "medialab"
---




*Ce billet fait partie d’une suite de billets sur le projet de refonte du site web du médialab de Sciences Po.*

Laboratoire de recherche interdisciplinaire réunissant sociologues, ingénieurs et designers, le médialab (SiencesPo) mène des recherches thématiques et méthodologiques exploitant et interrogeant la place prise par le numérique dans nos sociétés.

En octobre 2018, l’équipe du médialab nous contacte pour la refonte de leur site web. Le projet fut porté par une douzaine de personnes (designers, Ingénieurs, chercheurs, développeurs, chargée de communication et _facilitateur_) avec un noyau dur de cinq chevronné·es. 

Le résultat est en ligne et visible à cette adresse depuis septembre 2019: [medialab.sciencespo.fr](https://medialab.sciencespo.fr/). Quelques mois après sa sortie, nous allons revenir sur les coulisses de la conception artistique de ce projet dans une série de trois billets.

{{< img src="images/medialab_03.png" class="border" >}}


## Un projet collectif

Lorsque le médialab nous contacte, le projet n’a pas encore débuté. Leur volonté est de concevoir un site web statique, livré en continu, avec un travail d’intégration du côté des designers graphiques.

L’équipe du médialab prenant en charge le développement de l’architecture technique – un site statique ainsi qu’un éditeur de contenu taillé sur-mesure –, notre intervention devait être limitée à la conception artistique (design graphique et interactif) et à l’intégration de ces éléments. Mais à mesure des discussions et d’une confiance mutuelle installée, les frontières de cette dichotomie ont vite bougées.

En effet, loin de la simple déclinaison de la charte graphique de Sciences Po, notre regard de designer pu prendre part au renouveau de la stratégie éditoriale, de la structuration du site et des outils employés. La priorité étant ainsi donnée à la discussion, aux regards croisées et à la co-construction, le développement d’une _intelligence collective_ a pu se vérifier tout au long du projet. À ce titre notons la présence de [Thomas](https://oncletom.io) en _facilitateur_ qui a contribué à construire un climat d’écoute et de bienveillance.

Ainsi, avant d’aller plus en amont, actons que ce projet fut une entreprise éminemment plurielle et cette posture de travail fut enthousiasmante à vivre.


## Décroissance heureuse et *low-tech* 

Lors de notre première rencontre, l’équipe du médialab nous explique leurs attendus. Il s’agit pour eux de construire leur propre système technique au plus près de leurs besoins éditoriaux (grande diversité de production, trans- et multi-disciplinarité, sensibilité à l’open source et au partage d’outils, prédominance de l’approche collaborative, etc.) Vous pouvez en lire plus sur la démarche de l‘équipe dans le billet «&nbsp;[Redéfinir son identité publique numérique](https://medialab.sciencespo.fr/actu/redefinir-son-identite-publique-numerique/)&nbsp;» publié sur leur site.

Au fil des discussions émerge une envie à rebours de l’image de « vitrine technologique » auquel le médialab est facilement renvoyé. Une expression ressort particulièrement des échanges : « décroissance heureuse », soit des choix techniques restreints et durables, adaptés à leurs besoins et loin des bedonnantes usines à gaz de quelques CMS génériques bien connus. Ces quelques mots seront déterminants pour la suite du projet. 

Nous leur proposons alors de développer un site web dont l’esthétique et la technique s’inscriraient dans une philosophie *low tech* — soit une utilisation parcimonieuse des moyens à notre disposition et une prise en compte de leurs impacts. Nous présentons notre idée initiale à l’équipe du médialab en ces termes. 
Depuis, le mot est peu à peu apparu dans la communauté des web designers. De plus en plus de ressources à ce propos se concentrent sur des choix techniques pour optimiser les sites web et les rendre plus légers (voir le travail de [Gauthier Roussilhe](http://gauthierroussilhe.com/en/posts/convert-low-tech) ou le billet de framasoft [« Pour un web frugal »](https://framablog.org/2019/01/24/pour-un-web-frugal/)). 

Nos choix n’ont pas toujours été aussi radicaux que ceux présentés dans ces quelques billets; mais à défaut de savoir si *low tech* est le bon terme pour définir notre travail, cette philosophie nous a guidé dans nos décisions techniques et artistiques. 


## Techniques artistiques et vice versa

Le choix de la frugalité technologique a donc induit une discussion croisée entre les approches techniques et artistiques, cela en vue d’aboutir à une entité éditoriale cohérente.

D’un point de vue technique, le médialab a privilégié la construction d’une version robuste, stable et durable du site web qui évite les systèmes sophistiqués ou l’obsolescence programmée dû à certaines dépendances. Cela s’est traduit par le choix de construire un système spécifique au plus près des besoins et de développer un site statique, sans base de données et donc sans calcul du côté serveur.

Pour le *front end* notre ambition était de concevoir un site léger et peu gourmand en ressources, avec un minimum de _dette technique_. JQuery et autres librairies ou *frameworks* sont passés directement à la trappe. Puisque nous étions en si bon chemin, nous avons poussé l’idée jusqu’au bout en faisant totalement disparaître JavaScript du site en *front end*. Pour les interactions utilisateurs, nous avons donc privilégié l’usage de CSS avec quelques astuces (cf. 3e billet).

C’est finalement toute la direction artistique qui a suivi cette voie, convoquant un graphisme de la sobriété — peut être même de l’austérité tant il multiplie les clins d’œils à un temps où les ressources graphiques étaient considérablement limitées par la machine ( cf. [2e billet](/blog/2020-03-27_medialab-2/) ). Ainsi, les images illustratives sont remplacées par des suites de glyphes et les polices de caractères utilisées sont celles présentes par défaut sur les navigateurs web et les systèmes d’exploitation.

Dans les billets suivants, nous détaillerons la mise en application de ces principes. Un [billet](/blog/2020-03-27_medialab-2/) sera consacré au suivi de la direction artistique: _no fontes, no images, no javascript_. Le dernier [billet](/blog/2020-04-05_medialab-3/) sera un guide de survie pour des principes interactifs en pur CSS.

*Billet écrit à quatre mains par Julie et [Benjamin](http://benjmng.eu/)*



<video src="/projects/medialab/images/medialab_home.mp4" autoplay loop  class="border-video">
  Votre navigateur ne gère pas l’élément <code>video</code>.
</video>
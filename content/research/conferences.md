---
title: "Conférences (sélection)"
id: "conferences"
date: 2022-09-05T00:00:00+02:00
draft: false
---

- **Typesetting and Publishing Tools: Beyond WYSIWYG**&nbsp;[en] \
*Automatic Type Design 3*, Atelier National de Recherche Typographique (Nancy, France), 19 février 2025. 
    - [Video](https://vimeo.com/1058181662#t=56m10s)
- **Composer avec CSS Print**&nbsp;[fr] \
*Printemps de la typographie*, Institut National du Patrimoine (Paris, France), 21 mars 2024. 
    - [Slides](https://slides.julie-blanc.fr/20240322_printemps-typo.html)
- **Design graphique, code et recherche**&nbsp;[fr] \
*graphisme.design*, Université Paris 8  (Saint-Denis, France), 8 février 2024.
    - [Slides](https://slides.julie-blanc.fr/20240208_paris8.html)
    - Live Instagram: [partie 1](https://www.instagram.com/p/C3GLrvML05m/) - [partie 2](https://www.instagram.com/p/C3GRAZTrkrh/)
- **Vers des chaînes éditoriales légères et ouvertes**&nbsp;[fr] \
*11e journées du réseau Médici*, Liége (Belgique), 28 juin 2023.
    - [Slides](https://slides.julie-blanc.fr/20230628-Medici.html)
- **{ @media print }, A (bright) future without desktop publishing**&nbsp;[en] \
*About Design*, Paris College of Art (France), 14 mars 2023.
    - [Slides](https://slides.julie-blanc.fr/20230314_aboutdesign.html)
- **Transforming HTML to beautiful books and websites, a graphic designer point of view**&nbsp;[en] \
*Page Break*, San Francisco (États-Unis), 27-28 octobre 2022.
    - [Slides](https://slides.julie-blanc.fr/20221026_PageBreak.html#/cover)
- **Paginer le flux**&nbsp;[fr] \
*Rencontres EUR ArTeC*, Cité internationale des arts (Paris, France), 6&nbsp;octobre&nbsp;2022.
    - [Slides](http://slides.julie-blanc.fr/20221006_artec.html)
- **Composer une recherche en design graphique et en ergonomie&#8239;: une palette d'activités hétérogènes**&nbsp;[fr] \
Journée d'étude *Design graphique, manières de faire de la recherche*, Centre Pompidou (Paris, France), 17&nbsp;décembre&nbsp;2021.
    - [Slides](https://slides.julie-blanc.fr/20211215_composer-pompidou.html)
- **Visibilité du schème et hybridations instrumentales, l'introduction des technologie du web dans l'activité de composition paginée des designers graphiques** [fr] \
Colloque *Approche instrumentale, expansion et devenirs*, Université Paris 8 (Saint-Denis, France), 26 novembre 2021.
- **Intervenir au cœur des institutions&#8239;: tactiques et problématiques de designer-chercheur·euse·s** [fr] \
Conférence *Pratiques de la recherche en design* #4 [en ligne], 2 juin 2021.
    - [Informations](https://designenrecherche.org/actions/intervenir-au-coeur-des-institutions-tactiques-et-problematiques-de-designer-chercheur-euse-s/)
    - [Vidéo](https://www.youtube.com/watch?v=nrlafI2wuyk&ab_channel=DesignenRecherche)
- **@media print { book }** [fr] \
Ésad Valence – 8 mai 2021.
    - [Slides](https://slides.julie-blanc.fr/20210508_valence.html) 
- **Web-binders: layout of two printed and digital publications** [en] \
Julie Blanc et Nicolas Taffin, \
*Open Publishing Fest* [en ligne],  27 mai 2020.
    - [Vidéo](https://scalelite.cloud68.co/playback/presentation/2.0/playback.html?meetingId=5bb1cc79f0266843507b8c7e158b1a19ca2bc83b-1590591586485)
- **Présentation de paged.js pour le W3C Print CSS Workshop** [en] \
*XML Prague* (République tchèque), 13 février 2020.
    - [Position paper](https://lists.w3.org/Archives/Public/public-css-print/2020JanMar/att-0019/position-paper-w3C-workshop.html)
- **Présentation de la publication numérique et imprimée du catalogue des sculptures de la villa romaine de Chiragan** [fr] \
Julie Blanc et Christelle Molinié, \
*Les Lundis numériques de l'INHA* (Paris, France), 13 janvier 2020.
    - [Slides](http://slides.julie-blanc.fr/20200113_INHA-chiragan.html) / [Vidéo](https://www.youtube.com/watch?v=-bTGBPaJR3o&list=PLsl8NWzVv6T2CQFtBOfnlA_EKLFeCFSUG&index=29&t=0s)
- **Du fanzine à l’encyclopédie, éditions avec et après le numérique** [fr] \
Séminaire *Dé-brancher l’œuvre*, master ArTeC, Université Paris 8, 4 octobre 2019.
    - [Slides](http://slides.julie-blanc.fr/20191004_sem-artec-editions.html)
- **Print Books with Browsers** [en] \
*Digital Publishing Summit*, BnF (Paris, France), 26 mai 2019.
    - [Slides](http://slides.julie-blanc.fr/20190626_DigitalPublishingSummit.html) / [Vidéo](https://www.youtube.com/watch?v=3SvfARdZRA4)
- **Présentation de paged.js** [fr/en] \
Présentation au meeting *Write the docs*, Mozilla (Paris, France), 14 juin 2019.
    - [Slides](http://slides.julie-blanc.fr/20190613_write-the-doc.html) [en]
- **Faire des livres avec un navigateur** [fr] \
Julie Blanc et Julien Taquet, \
Conférence aux *Journées du Logiciel Libre*, Maison pour tous (Lyon, France), 6 avril 2019.
    - [Slides](http://slides.julie-blanc.fr/20190406_jdll.html)
- **Imprimé, single source publishing et technologies du web** [fr]\
Conférence au colloque *Repenser les humanités numériques*, CRIHN (Montréal, Canada), 27 octobre 2018.
    - [Slides](http://slides.julie-blanc.fr/20181027_crihn.html)
- **Paginer le flux** [fr]\
Présentation aux *Rencontres internationale de Lure* (Lurs, France), 20 août 2018.
    - [Slides](http://slides.julie-blanc.fr/20180820_Lure.html)
- **Design des publications scientifiques multisupports** [fr]\
Julie Blanc et Lucile Haute, \
Conférence au colloque international *ÉCRiDiL, Écrire, éditer, lire à l’ère numérique*, Usine C (Montréal, Canada), 3 avril 2018.
- **Éditer une revue “arts et sciences” aujourd’hui**[fr]\
Avec David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.\
Table ronde à l'évènement *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), 2 février 2018. 
- **PagedMedia approaches** [en]\
Présentation au *Paged Media Meeting*, MIT press (Cambridge, États-Unis), 9 janvier 2018.
- **(Re)Penser les chaînes de publication : soutenabilité et émancipation** [fr]\
Julie blanc et Antoine Fauchié, \
Conférence au colloque international *Les écologies du numérique*, ÉSAD Orléans – Écolab (Orléans, France), 10 novembre 2017.
- **Standards du web et publication académique** [fr]
Julie Blanc et Lucile Haute, \
Conférence à *PrePostPrint @ Gaîté Lyrique*, Paris, 21 octobre 2018.

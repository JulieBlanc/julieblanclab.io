---
title: "navigation"
id: "nav-activites"
draft: false
date: 2022-08-01T00:00:00+02:00
draft: false
---

<nav>
    <ul>
        <li><a href="#container-thesis">Thèse</a></li>
        <li><a href="#container-publications">Publications</a></li>
        <li><a href="#container-collectifs">Aventures collectives</a></li>
        <li><a href="#container-conferences">Conférences</a></li>
        <li><a href="#container-workshops">Ateliers</a></li>
        <li><a href="#container-organization">Organisation</a></li>
        <li><a href="#container-medias">Médias</a></li>
    </ul>
</nav>
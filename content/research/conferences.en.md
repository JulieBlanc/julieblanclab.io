---
title: "Conferences (selection)"
id: "conferences"
date: 2022-09-05T00:00:00+02:00
draft: false
---

- **Typesetting and Publishing Tools: Beyond WYSIWYG**&nbsp;[en] \
*Automatic Type Design 3*, Atelier National de Recherche Typographique (Nancy, France), February 19th, 2025. 
    - [Video](https://vimeo.com/1058181662#t=56m10s)
- **Composer avec CSS Print**&nbsp;[fr] \
*Printemps de la typographie*, Institut National du Patrimoine (Paris, France), March 21th, 2024. 
    - [Slides](https://slides.julie-blanc.fr/20240322_printemps-typo.html)
- **Design graphique, code et recherche**&nbsp;[fr] \
*graphisme.design*, Université Paris 8 (Saint-Denis, France), February 8th, 2024.
    - [Slides](https://slides.julie-blanc.fr/20240208_paris8.html)
    - Live Instagram: [partie 1](https://www.instagram.com/p/C3GLrvML05m/) - [partie 2](https://www.instagram.com/p/C3GRAZTrkrh/)
- **Vers des chaînes éditoriales légères et ouvertes**&nbsp;[fr] \
*11e journées du réseau Médici*, Liége (Belgium), June 28th 2023.
    - [Slides](https://slides.julie-blanc.fr/20230628-Medici.html)
- **{ @media print }, A (bright) future without desktop publishing**&nbsp;[en] \
*About Design*, Paris College of Art (Paris, France), March 14th, 2023.
    - [Slides](https://slides.julie-blanc.fr/20230314_aboutdesign.html)
- **Transforming HTML to beautiful books and websites, a graphic designer point of view**&nbsp;[en] \
*Page Break*, San Francisco, (CA USA), 27-28 October 2022.
    - [Slides](https://slides.julie-blanc.fr/20221026_PageBreak.html#/cover)
- **Paginer le flux**&nbsp;[fr] \
*Rencontres EUR ArTeC*, Cité internationale des arts (Paris, France), October&nbsp;6,&nbsp;2022.
    - [Slides](http://slides.julie-blanc.fr/20221006_artec.html)
- **Composer une recherche en design graphique et en ergonomie&#8239;: une palette d'activités hétérogènes** [fr] \
Seminar *Design graphique, manières de faire de la recherche*, Centre Pompidou (Paris, France), December 17, 2021.
    - [Slides](https://slides.julie-blanc.fr/20211215_composer-pompidou.html)
- **Visibilité du schème et hybridations instrumentales, l'introduction des technologie du web dans l'activité de composition paginée des designers graphiques** [fr] \
Conference at colloquium *Approche instrumentale, expansion et devenirs*, Université Paris 8 (Saint-Denis, France), November 26, 2021.
- **Intervenir au cœur des institutions : tactiques et problématiques de designer-chercheur·euse·s** [fr] \
Conference *Pratiques de la recherche en design* #4 \[online\], June 2, 2021.
    - [Informations](https://designenrecherche.org/actions/intervenir-au-coeur-des-institutions-tactiques-et-problematiques-de-designer-chercheur-euse-s/)
    - [Video](https://www.youtube.com/watch?v=nrlafI2wuyk&ab_channel=DesignenRecherche)
- **@media print { book }** [fr] \
Ésad Valence – 8 mai 2021.
    - [Slides](https://slides.julie-blanc.fr/20210508_valence.html)
- **Web-binders: layout of two printed and digital publications** [en] \
Julie Blanc and Nicolas Taffin, \
*Open Publishing Fest* \[online\], May 27, 2020.
    - [Video](https://scalelite.cloud68.co/playback/presentation/2.0/playback.html?meetingId=5bb1cc79f0266843507b8c7e158b1a19ca2bc83b-1590591586485)
- **Presentation of paged.js at the W3C Print CSS Workshop** [en] \
*XML Prague* (Czech Republic), February 13, 2020.
    - [Position paper](https://lists.w3.org/Archives/Public/public-css-print/2020JanMar/att-0019/position-paper-w3C-workshop.html)
- **Présentation de la publication numérique et imprimée du catalogue des sculptures de la villa romaine de Chiragan** [fr] \
Julie Blanc and Christelle Molinié, \
*Les Lundis numériques de l'INHA* (Paris, France), January 13, 2020.
    - [Slides](http://slides.julie-blanc.fr/20200113_INHA-chiragan.html) / [Video](https://www.youtube.com/watch?v=-bTGBPaJR3o&list=PLsl8NWzVv6T2CQFtBOfnlA_EKLFeCFSUG&index=29&t=0s)
- **Du fanzine à l’encyclopédie, éditions avec et après le numérique** [fr] \
Seminar *Dé-brancher l’œuvre*, master ArTeC, Université Paris 8, October 4, 2019.
    - [Slides](http://slides.julie-blanc.fr/20191004_sem-artec-editions.html)
- **Print Books with Browsers** [en] \
*Digital Publishing Summit*, BnF (Paris, France), May 26, 2019.
    - [Slides](http://slides.julie-blanc.fr/20190626_DigitalPublishingSummit.html) / [Video](https://www.youtube.com/watch?v=3SvfARdZRA4)
- **Paged.js presentation** [fr/en] \
Presentation at the meeting *Write the docs*, Mozilla (Paris, France), June 14, 2019.
    - [Slides](http://slides.julie-blanc.fr/20190613_write-the-doc.html) [en]
- **Faire des livres avec un navigateur** [fr] \
Julie Blanc and Julien Taquet, \
Conference at *Les Journées du Logiciel Libre*, Maison pour tous (Lyon, France), April 6, 2019.
    - [Slides](http://slides.julie-blanc.fr/20190406_jdll.html)
- **Imprimé, single source publishing et technologies du web** [fr]\
Conference at colloquium *Repenser les humanités numériques*, CRIHN (Montréal, Canada), Octobre 27, 2018.
    - [Slides](http://slides.julie-blanc.fr/20181027_crihn.html)
- **Paginer le flux** [fr]\
Presentation at *Rencontres internationale de Lure* (Lurs, France), August 20, 2018.
    - [Slides](http://slides.julie-blanc.fr/20180820_Lure.html)
- **Design des publications scientifiques multisupports** [fr]\
Julie Blanc et Lucile Haute, \
Conference at the international colloquium *ÉCRiDiL, Écrire, éditer, lire à l’ère numérique*, Usine C (Montréal, Canada), April 30, 2018.
- **Éditer une revue “arts et sciences” aujourd’hui**[fr]\
With David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.\
Panel discussion at the event *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), February 2, 2018. 
- **PagedMedia approaches** [en]\
Presentation at *Paged Media Meeting*, MIT press (Cambridge, États-Unis), January 9, 2018.
- **(Re)Penser les chaînes de publication : soutenabilité et émancipation** [fr]\
Julie Blanc and Antoine Fauchié, \
Conference at the international colloquium *Les écologies du numérique*, ÉSAD Orléans – Écolab (Orléans, France), November 10, 2017.
- **Standards du web et publication académique** [fr]
Julie Blanc and Lucile Haute, \
Conference at *PrePostPrint* @ Gaîté Lyrique, Paris, October 21, 2018.

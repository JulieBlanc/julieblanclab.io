---
title: "Workshops (selection)"
id: "workshops"
date: 2022-09-06T00:00:00+02:00
draft: false
---

- **Multisupport publishing with web technologies** \
SUPSI, Mendrisio (Swiss) – December 11–15, 2023.
    - [website](https://workshops.julie-blanc.fr/2023-supsi/) [en]
- **XPUB, Master of Arts in Fine Art and Design: Experimental Publishing** \
Piet Zwart Institute, Rotterdam (Netherland) – October, 13 2022.
- **Paged.js workshop at Cambrai** \
Ésac Cambrai (France) – October 27-29, 2021.
    - [website](https://workshops.julie-blanc.fr/2021-esac-cambrai/) [fr]
- **@media print { book }** \
Ésad Valence – May 10-12, 2021.
    - [slides](https://slides.julie-blanc.fr/20210508_valence.html) [fr]
- **Paged.js hackathon** \
École nationale supérieure des Arts Décoratifs, Paris (France), March 1st-2nd, 2021.
    - [blogpost](https://julie-blanc.fr/en/blog/2021-04-16_hackathon-pagedjs/) [en]
- **Les formes du web (Creation research module)** \
École nationale supérieure des Arts Décoratifs, Paris (France), 2020.
    - [website](http://workshops.julie-blanc.fr/2020-mrc-ensad/) [fr]
- **Scripter Gutenberg: des publication de papier et d'écran** \
    Grands Ateliers, ÉSAD Orléans (France), January 21-27, 2020.
    - [website](http://workshops.julie-blanc.fr/2020-esad-orleans/) [fr]
- **Markdown and Bibliography**  \
Design en Recherche, CRI (Centre de Recherche Interdisciplinaire), Paris (France), June 18, 2019. 
- **Création hybride avec des outils numériques libres** \
*Les écrits du numérique #4*, Alphabetville and la Marelle, Friche La Belle de Mai, Marseille (France), March 23, 2019.
- **Paged Media × PrePostPrint workshops** \
Open Source Publishing lab, Brussels (Belgium), November 28, 2018. \
École nationale supérieure des Arts Décoratifs, Paris (France), November 29 & 30, 2018.
    - [blogpost](https://web.archive.org/web/20190506055446/https://www.pagedmedia.org/an-overview-of-the-paged-media-x-prepostprint-workshop/) [en]
- **Open Hybridization (Creation research module)** \
École nationale supérieure des Arts Décoratifs, Paris (France), 2018, 2019.
    - [website 2019](http://workshops.julie-blanc.fr/libre-hybridation-2019/) [fr]
- **Écrire et  mettre en forme : quels formats, quels outils ?**  \
Design en Recherche, École nationale supérieure des Arts Décoratifs, Paris (France), July 5 ,2018.

---
title: "Publications"
id: "publications"
date: 2022-09-02T00:00:00+02:00
draft: false
---

- **«&#8239;Une recherche ‹par› et ‹pour› le design : un entretien croisé sur trois thèses de recherche en design réalisées dans le contexte académique français&#8239;»** [fr]\
Julie Blanc, Marion Voillot, et Élise Rigot, *Sciences du Design*, n° 20(2), 2025. 
    - sur [Cairn.info](https://shs.cairn.info/revue-sciences-du-design-2024-2-page-70?lang=fr&tab=texte-integral)
- **«&#8239;Collaborer à grande échelle dans des communautés de pratique en design graphique&#8239;»** [fr]\
Julie Blanc, *Our Collaborative tools*, 2024.
    - en ligne, sur [Our Collaborative tools](https://ourcollaborative.tools/fr/article/collaboration-in-graphic-design-commun)
- **«&#8239;Composer avec les technologies du web&#8239;: genèses instrumentales et apprentissage collectif au sein d'une communauté de designers graphiques dans le champ éditorial&#8239;»** [fr]\
Julie Blanc et Anne Bationo-Tillon, *TransFormations – Recherches en Éducation et Formation des Adultes*, 2023/1 (n° 25).
    - [PDF](https://transformations.univ-lille.fr/index.php/TF/article/view/498/281)
- **«&#8239;Code 〈–〉 Design graphique.  Dix ans de relations &#8239;»** [fr]\
Julie Blanc et Nolwenn Maudet, *Graphisme en France*, «&#8239;Création, outils, recherche&#8239;» (n° 28), 2022.
    - [PDF](https://www.cnap.fr/actualites/graphisme-en-france/revues/ndeg28-graphisme-en-france-creation-outils-recherche-2022)
- **«&#8239;Si Jan Tschichold avait connu les feuilles de style en cascade&#8239;: plaidoyer pour une mise en page comme programme&#8239;»** [fr]\
Julie Blanc, *Design Arts Médias*, dossier « Systèmes&nbsp;: logiques, graphies, matérialités », 2022.
    - en ligne, sur [Design Arts Médias](https://journal.dampress.org/issues/systemes-logiques-graphies-materialites/si-jan-tschichold-avait-connu-les-feuilles-de-style-en-cascade-plaidoyer-pour-une-mise-en-page-comme-programme)
- **« Technologies de l’édition numérique »** [fr]\
Julie Blanc et Lucile Haute, *Sciences du design*, 2018 /2 (n° 8), pp. 11-17.
    - [en ligne](http://recherche.julie-blanc.fr/timeline-publishing/), version augmentée en anglais
    - sur [Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm)
- **« Publier la recherche en design : (hors-)normes, (contre-)formats, (anti-)standards »** [fr] \
Julie Blanc et Lucile Haute, *Réel | Virtuel : enjeux du numérique*, n° 6, « Les normes du numériques », 2018.
    - en ligne, sur [Réel-Virtuel](http://www.reel-virtuel.com/numeros/numero6/sentinelles/publier-recherche-design)
- **« Standards du web et publication académique »** [fr]\
Julie Blanc et Lucile Haute, *Code X – 01 : PrePostPrint*, éditions HYX, 2017.

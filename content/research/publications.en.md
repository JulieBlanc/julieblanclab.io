---
title: "Publications"
id: "publications"
date: 2022-09-02T00:00:00+02:00
draft: false
---

- **«&#8239;Une recherche ‹par› et ‹pour› le design : un entretien croisé sur trois thèses de recherche en design réalisées dans le contexte académique français&#8239;»** [fr]\ 
Julie Blanc, Marion Voillot, et Élise Rigot, *Sciences du Design*, n° 20(2), 2025.      
    - on [Cairn.info](https://shs.cairn.info/revue-sciences-du-design-2024-2-page-70?lang=fr&tab=texte-integral)
- **«&#8239;Large-scale collaboration in graphic design communities of practice&#8239;»** [en]\
Julie Blanc, *Our Collaborative tools*, 2024.
    - Online: [english](https://ourcollaborative.tools/en/article/collaboration-in-graphic-design-commun), translation coming soon / [french](https://ourcollaborative.tools/fr/article/collaboration-in-graphic-design-commun)
- **“Composer avec les technologies du web&#8239;: genèses instrumentales et apprentissage collectif au sein d'une communauté de designers graphiques dans le champ éditorial“** [fr]\
Julie Blanc et Anne Bationo-Tillon, *TransFormations – Recherches en Éducation et Formation des Adultes*, 2023/1 (n° 25).
    - [PDF](https://transformations.univ-lille.fr/index.php/TF/article/view/498/281)
- **«&#8239;Code 〈–〉 Design graphique.  Dix ans de relations &#8239;»** [fr]\
Julie Blanc et Nolwenn Maudet, *Graphisme en France*, «&#8239;Création, outils, recherche&#8239;» (n° 28), 2022.
    - [PDF](https://www.cnap.fr/actualites/graphisme-en-france/revues/ndeg28-graphisme-en-france-creation-outils-recherche-2022)
- **“Si Jan Tschichold avait connu les feuilles de style en cascade&nbsp;: plaidoyer pour une mise en page comme programme”** [fr]\
Julie Blanc, *Design Arts Médias*, dossier « Systèmes&nbsp;: logiques, graphies, matérialités », 2022.
    - [online](https://journal.dampress.org/issues/systemes-logiques-graphies-materialites/si-jan-tschichold-avait-connu-les-feuilles-de-style-en-cascade-plaidoyer-pour-une-mise-en-page-comme-programme)
- **“Timeline of technologies for publishing”** [fr]\
Julie Blanc and Lucile Haute, *Sciences du design*, 2018 /2 (n° 8), pp. 11-17.
    - [online](http://recherche.julie-blanc.fr/timeline-publishing/), english augmented version
    - on [Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm)
- **“Publier la recherche en design: (hors-)normes, (contre-)formats, (anti-)standards”** [fr] \
Julie Blanc and Lucile Haute, *Réel | Virtuel: enjeux du numérique*, n° 6, “Les normes du numériques”, 2018.
    - on [Réel-Virtuel](http://www.reel-virtuel.com/numeros/numero6/sentinelles/publier-recherche-design)
- **“Web standards and academic publishing”** [fr]\
Julie Blanc et Lucile Haute, *Code X – 01: PrePostPrint*, éditions HYX, 2017.

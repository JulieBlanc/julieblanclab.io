---
title: "navigation"
id: "nav-activites"
draft: false
date: 2022-08-01T00:00:00+02:00
draft: false
---

<nav>
    <ul>
        <li><a href="#container-thesis">PhD</a></li>
        <li><a href="#container-publications">Publications</a></li>
        <li><a href="#container-collectifs">Collective adventures</a></li>
        <li><a href="#container-conferences">Conferences</a></li>
        <li><a href="#container-workshops">Workshops</a></li>
        <li><a href="#container-organization">Organization</a></li>
        <!-- <li><a href="#container-medias">Médias</a></li> -->
    </ul>
</nav>
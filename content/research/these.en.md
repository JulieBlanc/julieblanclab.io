---
title: "PhD"
id: "thesis"
draft: false
date: 2022-09-01T00:00:00+02:00
draft: false
---

**Composer avec les technologies du web. Genèses instrumentales collectives pour le développement d’une communauté de pratique de designers graphiques**, Thèse de doctorat en ergonomie, Université Paris 8, Vincennes – Saint Denis, 2023. Français. <a href="https://phd.julie-blanc.fr/" target="_blank">⟨phd.julie-blanc.fr⟩</a><br><br>

- Doctorat mené sous la direction de Anne Bationo-Tillon, la codirection de Samuel Bianchini et le co-encadrement de Lucile Haute, soutenue par l’EUR ArTeC et préparée à l’université Paris 8 au sein du laboratoire EA349 Paragraphe, équipe C3U et à l’École nationale supérieure des Arts Décoratifs - PSL, au sein d’EnsadLab, équipe Reflective Interaction.
    - [phd.julie-blanc.fr](https://phd.julie-blanc.fr)
    - [HAL](https://hal.science/tel-04467286)
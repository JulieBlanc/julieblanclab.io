---
title: "Aventures collectives"
id: "collectifs"
date: 2022-09-03T00:00:00+02:00
draft: false
---
- **Les rencontres de Lure** \
Association qui examine l’écriture, son évolution et ses formes (typographique, littéraire, visuelle ou numérique).  
    - <a href="https://delure.org/" target="_blank">delure.org</a>
- **PrePostPrint** \
Communauté qui cultive un intérêt pour les procédés de création graphique et les systèmes de publication libres considérés comme « alternatifs » ou « non conventionnels », particulièrement s’ils sont conçus avec les technologies du web. 
    - <a href="https://prepostprint.org/" target="_blank">prepostprint.org</a>
- **Paged.js (Coko Foundation)** (past) \
Librairie JavaScript open-source pour permettre l’export de PDF et l’affichage paginé dans le(s) navigateur(s) à partir d’HTML et CSS
    - <a href="https://www.pagedjs.org/" target="_blank">pagedjs.org</a>
- **Design en Recherche** (past)\
Réseau de doctorant-e-s et jeunes docteur-e-s dont les recherches portent sur les thématiques émergentes d’étude et de pratique du design
    - <a href="http://designenrecherche.org/" target="_blank">designenrecherche.org</a>

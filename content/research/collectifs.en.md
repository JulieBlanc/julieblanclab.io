---
title: "Collective adventures"
id: "collectifs"
date: 2022-09-03T00:00:00+02:00
draft: false
---

- **Les rencontres de Lure** \
examines writing in all its forms (typography, literature, visual or digital)
    - <a href="https://delure.org/en/lassociation/a-propos" target="_blank">delure.org</a>
- **PrePostPrint** \
Group of people who meets at events for discuss on alternative free publishing,  particularly interested in the creation of hybrid and printed publications with web technology
    - <a href="https://prepostprint.org/" target="_blank">prepostprint.org</a>
- **Paged.js (Coko Foundation)** (past) \
Open-source javascript library to paginate HTML/CSS in the browser, and to apply PagedMedia controls to paginated content for the purposes of exporting print-ready, or display-friendly, PDF from the browser
    - <a href="https://www.pagedjs.org/" target="_blank">pagedjs.org</a>
- **Design en Recherche** (past) \
Network of Ph.D students and young Ph.D whose research focuses on emerging design study and practice
    - <a href="http://designenrecherche.org/" target="_blank">designenrecherche.org</a>


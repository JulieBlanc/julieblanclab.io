---
title: "Ateliers (selection)"
id: "workshops"
date: 2022-09-06T00:00:00+02:00
draft: false
---

- **Multisupport publishing with web technologies** \
SUPSI, Mendrisio (Suisse) –  11–15 décembre 2023.
    - [site web des travaux](https://workshops.julie-blanc.fr/2023-supsi/) [en]
- **XPUB, Master of Arts in Fine Art and Design: Experimental Publishing** \
Piet Zwart Institute, Rotterdam (Pays-Bas) – 13 octobre 2022.
- **Paged.js workshop à Cambrai** \
Ésac Cambrai – 27-29 octobre 2021.
    - [site web des travaux](https://workshops.julie-blanc.fr/2021-esac-cambrai/) [fr]
- **@media print { book }** \
Ésad Valence – 10-12 mai 2021.
    - [slides conférence](https://slides.julie-blanc.fr/20210508_valence.html) [fr]
- **Paged.js hackathon** \
École nationale supérieure des Arts Décoratifs, Paris (France), 1er-2 mars 2021.
    - [billet de blog](https://julie-blanc.fr/blog/2021-04-16_hackathon-pagedjs/) [en]
- **Les formes du web (module de recherche création)** \
École nationale supérieure des Arts Décoratifs, Paris (France), 2020.
    - [site web](http://workshops.julie-blanc.fr/2020-mrc-ensad/) [fr]
- **Scripter Gutenberg: des publication de papier et d'écran** \
Grands Ateliers, ÉSAD Orléans (France), 21-27 janvier 2020.
    - [site web](http://workshops.julie-blanc.fr/2020-esad-orleans/) [fr]
- **Markdown et Bibliographie** \
Design en Recherche, CRI (Centre de Recherche Interdisciplinaire), Paris (France), 18 juin 2019.
- **Création hybride avec des outils numériques libres** \
*Les écrits du numérique #4*, Alphabetville et la Marelle, Friche La Belle de Mai, Marseille (France), 23 mars 2019.
- **Paged Media × PrePostPrint workshops** \
Open Source Publishing lab, Bruxelles (Belgique), 28 novembre 2018. \
École nationale supérieure des Arts Décoratifs, Paris (France), 29 et 30 novembre 2018.
    - [billet de blog](https://web.archive.org/web/20190506055446/https://www.pagedmedia.org/an-overview-of-the-paged-media-x-prepostprint-workshop/) [en]
- **Libre Hybridation (module de recherche création)** \
École nationale supérieure des Arts Décoratifs, Paris (France), 2018, 2019.
    - [site web 2019](http://workshops.julie-blanc.fr/libre-hybridation-2019/) [fr]
- **Écrire et  mettre en forme : quels formats, quels outils ?** \
Design en Recherche, École nationale supérieure des Arts Décoratifs, Paris (France), 5 juillet 2018.

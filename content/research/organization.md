---
title: "Organisation"
id: "organization"
date: 2022-09-07T00:00:00+02:00
draft: false
---

- **Pratiques de la recherche en design** [fr] \
Cycle de conférence de l'association Design en Recherche, 2021.
- **Manières de voir, manières de penser le monde. Méthodologie(s) à l’épreuve du réel et pratiques (pluri-)disciplinaires** [fr] \
Colloque CLI 2021, Université Paris 8 [en ligne], 4 mars 2021. 
    - [site web](https://colloque-cli.univ-paris8.fr/index.html)
- **EnsadLab invite PrePostPrint : Enjeux des systèmes de publication libres et outils alternatifs pour la création graphique** [fr] \
Colloque, EnsAD (Paris), 3–4 avril 2018. 
    - [Présentation et vidéos](http://www.ensadlab.fr/fr/francais-prepostprint/)
- **Éditer une revue “arts et sciences” aujourd’hui**[fr] \
Table ronde à l'évènement *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), 2 février 2018. \
Avec David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.
- **PrePostPrint @ Gaîté Lyrique** [fr] \ 
Conférences et salon de l’édition, Gaîté Lyrique (Paris), 20–21 octobre 2017.

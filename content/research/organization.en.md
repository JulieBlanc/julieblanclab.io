---
title: "Organization"
id: "organization"
date: 2022-09-07T00:00:00+02:00
draft: false
---

- **Pratiques de la recherche en design** [fr] \
Conference cycle of the association *Design en Recherche*, 2021.
- **Manières de voir, manières de penser le monde. Méthodologie(s) à l’épreuve du réel et pratiques (pluri-)disciplinaires** [fr] \
Colloquium CLI 2021, Université Paris 8 [online], March 4, 2021. 
    - [Website](https://colloque-cli.univ-paris8.fr/index.html)
- **EnsadLab invite PrePostPrint : Enjeux des systèmes de publication libres et outils alternatifs pour la création graphique** [fr]\
Colloquium, EnsAD (Paris), April 3–4, 2018. 
    - [Website and videos](http://www.ensadlab.fr/fr/francais-prepostprint/)
- **Éditer une revue “arts et sciences” aujourd’hui**[fr]\
Panel discussion at the event *Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), February 2, 2018.\
With David Bihanic, Lucile Haute, Anthony Masure, Robin de Mourat, Vincent Piccolo, Annick Rivoire, Nolwenn Tréhondart.
- **PrePostPrint @ Gaîté Lyrique** [fr] \ 
 Conferences and publishing exhibition, Gaîté Lyrique (Paris), October 20–21, 2017. 

---
title: "About"
date: 2019-09-15T10:41:14+02:00
draft: false
---


I'm a graphic designer and CSS developer. I&nbsp;use web technologies (HTML5, CSS3, javascript, epub) to&nbsp;design all kinds of publications, both digital and printed. I&nbsp;build tools for multisupport editorial workflows, adapted to each project and facilitating collaboration. I use to contribute to the development of Paged.js<a href="https://www.pagedjs.org/" target="_blank">⟡</a>, a&nbsp;tool to&nbsp;use web technologies for printing.


I'm also a researcher with a PhD in ergonomics and design from the University of Paris 8. My thesis<a href="https://phd.julie-blanc.fr/" target="_blank">⟡</a> is about the use of web technologies for printing by graphic designers.


<!-- I am a graphic designer and researcher. I&nbsp;am&nbsp;preparing a&nbsp;PhD in&nbsp;ergonomics and design graphic at&nbsp;<em>Paragraphe</em> Laboratory (University of Paris&nbsp;8) and at EnsadLab-Paris in the framework of ArTeC university research school (Art,&nbsp;Technologies and Creation). I&nbsp;contribute to the development of&nbsp;Paged.js[⟡](#section-research) and participate in&nbsp;PrePostPrint[⟡](#section-research). 

My work and research focuses on the&nbsp;recent shift from desktop publishing software to the use of web development technologies and methods (HTML5, CSS3, javascript, epub) in&nbsp;editorial workflows and particularly in&nbsp;graphic design practices. I&nbsp;also look at the implications of&nbsp;“automated” typesetting on graphic&nbsp;design.  


I’m&nbsp;also a &nbsp;researcher. I'm&nbsp;preparing a&nbsp;PhD in ergonomics and design graphic at&nbsp;Paragraphe Laboratory (University of Paris&nbsp;8) and at&nbsp;EnsadLab-Paris in the framework of&nbsp;ArTeC university research school (Art, Technologies and&nbsp;Creation).
-->
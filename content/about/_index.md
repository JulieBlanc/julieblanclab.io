---
title: "About"
date: 2019-09-15T10:41:14+02:00
draft: false
---

Je suis designer graphique et&nbsp;développeuse CSS. J’utilise les technologies du&nbsp;web (HTML5, CSS3, javascript, epub) pour concevoir toutes sortes de&nbsp;publications, numériques et imprimées. Je&nbsp;propose des outils permettant de&nbsp;construire des chaînes de publications légères, multisupports, adaptées à&nbsp;chaque projet et&nbsp;facilitant la collaboration. J’ai&nbsp;contribué au&nbsp;développement de&nbsp;Paged.js<a href="https://www.pagedjs.org/" target="_blank">⟡</a>, un&nbsp;outil permettant d’utiliser les technologies du&nbsp;web pour l’impression.

Je suis aussi chercheuse, docteure en ergonomie et design de l'Université Paris 8. Ma thèse<a href="https://phd.julie-blanc.fr/" target="_blank">⟡</a> porte sur l'utilisation des technologies du web pour l'impression par les designers graphiques. 



<!-- Je suis designer graphique et chercheuse. Je prépare une thèse en&nbsp;ergonomie et design graphique au laboratoire Paragraphe (Université Paris&nbsp;8) et à EnsadLab-Paris dans le cadre de l’école de&nbsp;recherche universitaire ArTeC (Art, Technologies et&nbsp;Création). Je&nbsp;contribue au développement de Paged.js[⟡](#section-research) et participe à&nbsp;PrePostPrint[⟡](#section-research).

Mes travaux et recherches portent essentiellement sur l'étude du&nbsp;récent passage des logiciels de Publication Assistée par&nbsp;Ordinateur à&nbsp;l’utilisation de&nbsp;technologies et de&nbsp;méthodes du&nbsp;développement web (HTML5, CSS3, javascript, epub) dans les&nbsp;chaînes éditoriales et tout particulièrement dans les pratiques du design graphique. J’aborde ainsi les implications de&nbsp;la&nbsp;composition «&nbsp;automatisée&nbsp;» sur les formes sensibles. -->
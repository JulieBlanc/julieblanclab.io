---
title: "Héro·ïne·s"
date: 2020-06-26T20:12:28+02:00
draft: false
publish: true
id: "heroines"
---

*Héro·ïne·s. Repenser les héros dans les œuvres et pratiques artistiques* (*Héro·ïne·s. Rethinking Heroes in Artistic Works and Practices*) is a publication of the Fémis research group, realized within the program "Sciences, Arts, Creation, Research" (SACRe, PSL). Following a two-day workshop with the research group, we designed a web publication that graphically and interactively presents each of the contributions and their multimedia content.<br>
The website was developed with the Hugo static site generator, the source code is on Gitlab and was deployed with Netlify. In order for members to easily edit the content from a graphical interface, I also plugged the site into Forestry.io.

- Website: [repenserlesheros.femis.fr/](https://repenserlesheros.femis.fr/)
- January 2021 - September 2021
- Source code on [GitLab](https://gitlab.com/JulieBlanc/publication-heros)


{{< img src="images/mockup-heros.jpg" >}}

{{< img src="images/mockup-pad.jpg" >}}

{{< img src="images/mockup-phone.jpg" >}}


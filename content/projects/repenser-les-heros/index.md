---
title: "Héro·ïne·s"
date: 2020-06-26T20:12:28+02:00
draft: false
publish: true
id: "heroines"
---


*Héro·ïne·s. Repenser les héros dans les œuvres et pratiques artistiques* est une publication du groupe de recherche de La Fémis, réalisée au sein du programme « Sciences, Arts, Création, Recherche » (SACRe, PSL). Suite à un atelier de deux jours proposé au groupe de recherche, nous avons conçu une publication web qui met en forme de manière graphique et interactive chacune des contributions et leurs contenus multimédias.<br>
Le site a été développé avec le générateur de site statique Hugo, le code source est sur Gitlab et a été déployé avec Netlify. Afin que les membres puissent éditer facilement le contenu depuis une interface graphique, j'ai branché le site sur Forestry.io.

- Site web: [repenserlesheros.femis.fr/](https://repenserlesheros.femis.fr/)
- Janvier 2021 - Septembre 2021
- Code source sur [GitLab](https://gitlab.com/JulieBlanc/publication-heros)

{{< img src="images/mockup-heros.jpg" >}}

{{< img src="images/mockup-pad.jpg" >}}

{{< img src="images/mockup-phone.jpg" >}}

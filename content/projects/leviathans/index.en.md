---
title: "Des léviathans"
id: "leviathans"
date: 2019-09-14T22:59:23+02:00
draft: false
publish: true
aliases:
    - /projets/leviathans.html
---

*Des léviathans* is an interdisciplinary book that brings together original texts on the theme of relations between societies and oceans. Crossed approaches include natural sciences, arts, humanities and others.

- Association Périophtalme
- 130 × 190 mm, 214 pages
- 100 copies printed in a risograph

{{< img src="images/leviathans_01.jpg" >}}
{{< img src="images/leviathans_02.jpg" >}}
{{< img src="images/leviathans_03.jpg" >}}
{{< img src="images/leviathans_04.jpg" >}}
{{< img src="images/leviathans_05.jpg" >}}
{{< img src="images/leviathans_06.jpg" >}}
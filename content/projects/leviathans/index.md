---
title: "Des léviathans"
id: "leviathans"
date: 2019-09-14T22:59:23+02:00
draft: false
publish: true
aliases:
    - /projets/leviathans.html
---

*Des léviathans* est un ouvrage interdisciplinaire qui rassemble des textes originaux autour du thème des relations entre sociétés et océans. Les approches croisées intègrent notamment les sciences naturelles, les arts, les sciences humaines sociales et autres.

- Association Périophtalme
- 130 × 190 mm, 214 pages
- 100 exemplaires imprimés en risographie

{{< img src="images/leviathans_01.jpg" >}}
{{< img src="images/leviathans_02.jpg" >}}
{{< img src="images/leviathans_03.jpg" >}}
{{< img src="images/leviathans_04.jpg" >}}
{{< img src="images/leviathans_05.jpg" >}}
{{< img src="images/leviathans_06.jpg" >}}
---
title: "Code X – PrePostPrint"
date: 2019-09-15T19:06:20+02:00
draft: false
publish: true
id: "cahier-erg"
---

The first issue of the "Code X" newspaper was published on the occasion of the [PrePostPrint](https://prepostprint.org/doku.php//fr/introduction) event of 20-21 October 2017 at Gaîté Lyrique (Paris). It was entirely designed in HTML and CSS from a web browser. The properties CSS region, CSS grid and CSS multicolumn have been used. The source files of the newspaper are available on [gitlab](https://gitlab.com/prepostprint/code-x-01).

- Designed and developed in collaboration with [Quentin Juhel](https://juhel-quentin.fr/)
- Publisher: éditions HYX
- 260 × 360 mm, 16 pages
- 500 copies
- [Buy the newspaper](http://www.editions-hyx.com/fr/code-x)

{{< img src="images/code-x_01.jpg" >}}
{{< img src="images/code-x_02.jpg" >}}
{{< img src="images/code-x_03.png" >}}
{{< img src="images/code-x_04.png" >}}
{{< img src="images/code-x_05.png" >}}
{{< img src="images/code-x_06.png" >}}
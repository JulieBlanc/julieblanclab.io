---
title: "Code X – PrePostPrint"
date: 2019-09-15T19:06:20+02:00
draft: false
publish: true
id: "cahier-erg"
aliases:
    - /projets/cahier-erg.html
---

Le premier numéro du journal « Code X » a été publié à l’occasion du salon de l'édition [PrePostPrint](https://prepostprint.org/doku.php//fr/introduction) du 20-21 octobre 2017 à la Gaîté Lyrique (Paris). Il a été entièrement conçu en HTML et CSS depuis un navigateur web. Les propriétés CSS region, CSS grid et CSS multicolumn ont été utilisées. Les fichiers sources du journal sont diponibles sur [gitlab](https://gitlab.com/prepostprint/code-x-01).

- Conçu et développé en collaboration avec [Quentin Juhel](https://juhel-quentin.fr/)
- Éditeur : éditions HYX
- 260 × 360 mm, 16 pages
- 500 exemplaires
- [Acheter le journal](http://www.editions-hyx.com/fr/code-x)

{{< img src="images/code-x_01.jpg" >}}
{{< img src="images/code-x_02.jpg" >}}
{{< img src="images/code-x_03.png" >}}
{{< img src="images/code-x_04.png" >}}
{{< img src="images/code-x_05.png" >}}
{{< img src="images/code-x_06.png" >}}
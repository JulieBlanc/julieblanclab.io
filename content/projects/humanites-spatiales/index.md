---
title: "Humanités Spatiales"
id: "humanites-spatiales"
date: 2019-09-14T18:04:21+02:00
draft: false
publish: true
aliases:
    - /projets/humanites-spatiales.html
---



Proposition d’une édition autour du site web [humanites-spatiales.fr](http://humanites-spatiales.fr/). « Humanités spatiales » est un carnet de recherche qui propose un espace d’analyse, de réflexion et de dialogue aux chercheurs en sciences humaines et sociales qui s’intéressent, ponctuellement ou régulièrement, à l’espace et aux activités spatiales, en particulier à ses diverses représentations culturelles, qu’elles soient scientifiques, artistiques, littéraires, visuelles, sonores, etc.

- 144 × 212 mm, 148 pages
- Direction éditoriale : Observatoire de l’Espace du CNES
- Responsable éditoriale : Catherine Radtka, ISCC
- Basé sur [humanites-spatiales.fr](http://humanites-spatiales.fr/)

{{< img src="images/humanites-spatiales-01.jpg" alt="Photographie du livre">}}
{{< img src="images/humanites-spatiales-02.jpg" alt="Photographie du livre">}}
{{< img src="images/humanites-spatiales-03.jpg" alt="Photographie du livre">}}
{{< img src="images/humanites-spatiales-04.jpg" alt="Photographie du livre">}}
{{< img src="images/humanites-spatiales-05.jpg" alt="Photographie du livre">}}

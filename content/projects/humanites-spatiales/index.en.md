---
title: "Humanités Spatiales (Space Humanities)"
id: "humanites-spatiales"
date: 2019-09-14T18:04:21+02:00
draft: false
publish: true
aliases:
    - /projets/humanites-spatiales.html
---


Proposal for an edition around the website [humanites-spatiales.fr](http://humanites-spatiales.fr/). "Humanités spatiales" (Space Humanities) is a research notebook that offers a space for analysis, reflection and dialogue to researchers in the humanities and social sciences who are interested, punctually or regularly, in space and space activities, particularly in its various cultural representations, whether scientific, artistic, literary, visual, sound, etc.

- 144 × 212 mm, 148 pages
- Editorial Direction: Observatoire de l’Espace du CNES
- Editorial Manager: Catherine Radtka, ISCC
- Based on [humanites-spatiales.fr](http://humanites-spatiales.fr/)

{{< img src="images/humanites-spatiales-01.jpg" alt="Picture of the book">}}
{{< img src="images/humanites-spatiales-02.jpg" alt="Picture of the book">}}
{{< img src="images/humanites-spatiales-03.jpg" alt="Picture of the book">}}
{{< img src="images/humanites-spatiales-04.jpg" alt="Picture of the book">}}
{{< img src="images/humanites-spatiales-05.jpg" alt="Picture of the book">}}
---
title: "Médialab"
date: 2019-12-23T19:12:28+02:00
draft: false
publish: true
id: "medialab"
---

In October 2018, the médialab team (interdisciplinary research labo) contacted us to redesign their website. The Médialab team takes charge of the development of a new technical architecture with a static site and their own content editor. We accompany them for the artistic direction of the website and then develop the idea of a site without javascript, fonts and images - a “low-tech” site. <br>
In this collective project, we produced the graphic and interactive design as well as the HTML and CSS code for the integration.



- Website: [medialab.sciencespo.fr](https://medialab.sciencespo.fr/)
- Developed in collaboration with [Benjamin Gremillon](http://benjmng.eu/) and the médialab trem
- October 2018 - September 2019
- Source code on [GitHub](https://github.com/medialab/website)


{{< img src="images/medialab_01.png" >}}
{{< img src="images/medialab_02.jpg" >}}
{{< img src="images/medialab_03.png" class="border" >}}

<video src="/videos/medialab_home.mp4" autoplay loop playsinline class="border-video">
</video>

<video src="/videos/medialab_page.mp4" autoplay loop  playsinline class="border-video">
</video>

<video src="/videos/medialab_liste.mp4" autoplay loop  playsinline class="border-video">
</video>
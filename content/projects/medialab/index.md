---
title: "Médialab"
date: 2019-12-23T19:12:28+02:00
draft: false
publish: true
id: "medialab"
---

En octobre 2018, l’équipe du Médialab (laboratoire de recherche interdisciplinaire sur les relations entre le&nbsp;numérique et nos sociétés) nous contacte pour la refonte de leur site web. Ils nous font part de leur envie&nbsp;: une approche par la « décroissance heureuse ». Nous les accompagnons pour la direction artistique de leur site web statique et développons alors l’idée d’un site sans javascript, sans fontes et&nbsp;sans image –&nbsp;un&nbsp;site « low-tech ». <br>
Dans ce projet collectif, nous avons produit la conception graphique et interactive ainsi que le code HTML et CSS pour l’intégration.


- Site web: [medialab.sciencespo.fr](https://medialab.sciencespo.fr/)
- Développé en collaboration avec [Benjamin Gremillon](http://benjmng.eu/) et l’équipe du médialab
- Octobre 2018 - septembre 2019
- Code source sur [GitHub](https://github.com/medialab/website)


{{< img src="images/medialab_01.png" >}}
{{< img src="images/medialab_02.jpg" >}}
{{< img src="images/medialab_03.png" class="border" >}}

<video src="/videos/medialab_home.mp4" autoplay loop playsinline class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

<video src="/videos/medialab_page.mp4" autoplay loop playsinline class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

<video src="/videos/medialab_liste.mp4" autoplay loop playsinline class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>
---
title: "Atlas Cosmologique"
date: 2019-09-15T18:58:57+02:00
draft: false
publish: true
id: "atlas-cosmos"
aliases:
    - /projets/atlas-cosmos.html
---

L’atlas cosmologique est un outil multisupport constitué de trois dispositifs : une interface web, des sessions de réflexions et des publications imprimées. Ce projet a pour objectif de matérialiser la manière dont une connaissance non objective se constitue et se structure à travers un corpus documentaire.

Sur le site internet, tous les utilisateurs peuvent ajouter une entrée dans l’atlas. Une interface commune leur permet de disposer les documents et des notes sous forme de différents blocs déplaçables à l’écran. L’atlas n’est jamais fixe ; ce montage dynamique et collaboratif révèle un sens subjectif toujours renouvelé grâce à la proximité spatiale des documents.

Les sessions de réflexions reposent sur le même principe, mais avec un groupe restreint d’utilisateurs et l’impression des documents. Elles permettent une dynamique différente grâce à la présence physique des participants et une durée limitée de réflexion. Les montages à l’issue de chaque session fixent ainsi l’instantanée d’une pensée commune.

Les publications imprimées sont distribuées à chaque participant à la fin des sessions. Elles permettent d’archiver le montage constitué : les documents utilisés sont fournis et un schéma du montage est reproduit sur la couverture. À ceci est ajoutée une retranscription de certains moments de la discussion et des photos documentant le travail collectif.

- Ce projet a été présenté en juin 2016 pour mon diplôme de master en « erg edit : politique du multiple »
- Interface web : [atlas-cosmologique.julie-blanc.fr](http://atlas-cosmologique.julie-blanc.fr/)


{{< img src="images/atlas-01.png" alt="Capture d'écran de l'interface web de l'atlas cosmologique">}}
{{< img src="images/atlas-02.jpg" alt="Capture d'écran de l'interface web de l'atlas cosmologique">}}
{{< img src="images/atlas-03.jpg" alt="Photographie de l'installation: documents imprimés, ordinateur avec l'interface web et imprimante pour le print-on-demand">}}
{{< img src="images/atlas-04.png" alt="Capture d'écran de l'interface web de l'atlas cosmologique">}}
{{< img src="images/atlas-05.jpg" alt="Photographie d'un workshop: feuilles disposées sur un mur">}}
{{< img src="images/atlas-06.jpg" alt="Exemple d'édition réalisée en print-on-demand suite à un workshop (pages intérieuresà">}}
{{< img src="images/atlas-07.jpg" alt="Exemple d'éditions réalisées en print-on-demand suite à des workshop">}}
{{< img src="images/atlas-08.jpg" alt="Photographie d'un workshop: feuilles disposées sur une table et sur un mur">}}
{{< img src="images/atlas-09.jpg" alt="Photographie d'un workshop: participants autour d'un ordinateur et d'une imprimante">}}
{{< img src="images/atlas-10.jpg" alt="Photographie d'un workshop: participant accrochant des feuilles au mur">}}
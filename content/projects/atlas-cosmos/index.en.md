---
title: "Atlas Cosmologique"
date: 2019-09-15T18:58:57+02:00
draft: false
publish: true
id: "atlas-cosmos"
aliases:
    - /projets/atlas-cosmos.html
---

The *Atlas Cosmologique* (Cosmological Atlas) is a multisupport tool consisting of three dispositifs: a web interface, reflection sessions and printed publications. This project aims to materialize the way in which non-objective knowledge is constituted and structured through a documentary corpus.

- This project was presented in June 2016 for my master's degree in “erg edit: politics of the multiple”
- Web interface:[atlas-cosmologique.julie-blanc.fr](http://atlas-cosmologique.julie-blanc.fr/)

{{< img src="images/atlas-01.png" alt="Capture d'écran de l'interface web de l'atlas cosmologique">}}
{{< img src="images/atlas-02.jpg" alt="Capture d'écran de l'interface web de l'atlas cosmologique">}}
{{< img src="images/atlas-03.jpg" alt="Photographie de l'installation: documents imprimés, ordinateur avec l'interface web et imprimante pour le print-on-demand">}}
{{< img src="images/atlas-04.png" alt="Capture d'écran de l'interface web de l'atlas cosmologique">}}
{{< img src="images/atlas-05.jpg" alt="Photographie d'un workshop: feuilles disposées sur un mur">}}
{{< img src="images/atlas-06.jpg" alt="Exemple d'édition réalisée en print-on-demand suite à un workshop (pages intérieuresà">}}
{{< img src="images/atlas-07.jpg" alt="Exemple d'éditions réalisées en print-on-demand suite à des workshop">}}
{{< img src="images/atlas-08.jpg" alt="Photographie d'un workshop: feuilles disposées sur une table et sur un mur">}}
{{< img src="images/atlas-09.jpg" alt="Photographie d'un workshop: participants autour d'un ordinateur et d'une imprimante">}}
{{< img src="images/atlas-10.jpg" alt="Photographie d'un workshop: participant accrochant des feuilles au mur">}}
---
title: "Les sculptures de la villa romaine de Chiragan"
date: 2020-03-19T19:12:28+02:00
draft: false
publish: true
id: "villa-chiragan"
---

Pour le musée Saint Raymond (Toulouse), Antoine Fauchié et moi avons développé un catalogue multi-support. Le catalogue présente une partie de la collection du musée consacrée à une ensemble de sculptures romaines découvertes au lieu-dit Chiragan à Martres-Tolosane. Inspiré par le Getty Museum de Los Angeles, nous avons conçu et développé une version numérique du catalogue et une version imprimée. Pour cela nous avons proposé une chaîne de publication crée spécifiquement pour l'équipe du musée et basée sur un ensemble d'outils open-source que nous avons assemblés (Jeckyll, Zotero, Forestry, Gitlab, paged.js, etc.)

<div style="height: var(--baseline)"></div>
{{< img src="images/responsive-chiragan-pages.jpg" class="border" >}}
<div style="height: var(--baseline)"></div>


Le design graphique et interactif du site a entièrement été développé en HTML, CSS et Javascript. La version imprimée utilise Paged.js pour générer le catalogue imprimé directement depuis le navigateur web. Ainsi, les contenus ne sont entrés et corrigés qu'une seule fois par le musée pour générer les deux versions. Le catalogue n'est pas encore imprimé mais devrait l'être dans les prochains mois.

- Catalogue numérique: [https://villachiragan.saintraymond.toulouse.fr/](https://villachiragan.saintraymond.toulouse.fr/)
- Développé en collaboration avec [Antoine Fauchié](https://www.quaternum.net/) et l’équipe du musée Saint Raymond
- Octobre 2018 - Janvier 2020
- Code source sur [GitLab](https://gitlab.com/musee-saint-raymond/villa-chiragan)
- Pour plus d'informations sur le projet, lire mon billet de blog « [Une chaîne de publication collaborative et multisupport pour le musée Saint-Raymond](https://julie-blanc.fr/blog/2020-11-05_chiragan/) » 


<!-- <p style="margin-top:calc(var(--baseline)*-0.5); margin-bottom:calc(var(--baseline)*-0.5)">Site web<p>

- [https://villachiragan.saintraymond.toulouse.fr/](https://villachiragan.saintraymond.toulouse.fr/)


<p style="margin-top:calc(var(--baseline)*-0.5); margin-bottom:calc(var(--baseline)*-0.5)">Catalogue imprimé<p>

- 165 × 235 mm, 168 pages
- 2000 exemplaires
- dos carré-collé cousu, CMJN + Pantone 876U -->

### Catalogue numérique


{{< img src="images/responsive-chiragan-cover.jpg" class="border" >}}

{{< img src="images/screenshot-site-1.png" class="border" >}}
{{< img src="images/screenshot-site-2.png" class="border" >}}

<video src="/videos/chiragan-web.mp4" autoplay loop playsinline class="border-video" poster="images/chiragan-web_poster.png">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

### Catalogue imprimé


- 165 × 235 mm, 168 pages
- 2000 exemplaires
- dos carré-collé cousu, CMJN + Pantone 876U

<video style="margin-top: 0;" src="/videos/video-book_chiragan.mp4" autoplay loop playsinline class="border-video" poster="images/video-book_chiragan_poster.png">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>

{{< img src="images/book-chiragan-1.jpg" >}}

{{< img src="images/chemin-fer_part2.png" class="border" >}}

{{< img src="images/print-inspector-1.png" class="border" >}}
{{< img src="images/print-inspector-2.png" class="border" >}}


{{< img src="images/book-chiragan-3.jpg" >}}
{{< img src="images/book-chiragan-4.jpg" >}}

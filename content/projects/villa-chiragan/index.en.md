---
title: "Les sculptures de la villa romaine de Chiragan"
date: 2020-03-19T19:12:28+02:00
draft: false
publish: true
id: "villa-chiragan"
---

Antoine Fauchié and I developed a multi-media catalogue for the Saint Raymond Museum (Toulouse, France). The catalogue presents a part of the museum's collection devoted to a group of Roman sculptures discovered at the Chiragan site in Martres-Tolosane. Inspired by the Getty Museum in Los Angeles, we designed and developed a digital version of the catalogue and a printed version. For this we proposed a workflow created specifically for the museum team and based on a set of open-source tools that we assembled (Jeckyll, Zotero, Forestry, Gitlab, paged.js, etc.).


<div style="height: var(--baseline)"></div>
{{< img src="images/responsive-chiragan-pages.jpg" class="border" >}}
<div style="height: var(--baseline)"></div>


The graphic and interactive design of the site has been entirely developed in HTML, CSS and Javascript. The printed version uses Paged.js to generate the printed catalogue directly from the web browser. The contents are entered and corrected only once by the museum to generate both versions. The catalogue is not yet printed but should be printed in the next few months.



- Site web: [https://villachiragan.saintraymond.toulouse.fr/](https://villachiragan.saintraymond.toulouse.fr/)
- Developed in collaboration with [Antoine Fauchié](https://www.quaternum.net/) et l’équipe du musée Saint Raymond
- October 2018 - January 2020
- Source code on [GitLab](https://gitlab.com/musee-saint-raymond/villa-chiragan)
- For more information on the project, see my blogpost (in French): « [Une chaîne de publication collaborative et multisupport pour le musée Saint-Raymond](https://julie-blanc.fr/blog/2020-11-05_chiragan/) » 


### Web catalogue

{{< img src="images/responsive-chiragan-cover.jpg" class="border" >}}

{{< img src="images/screenshot-site-1.png" class="border" >}}
{{< img src="images/screenshot-site-2.png" class="border" >}}

<video src="/videos/chiragan-web.mp4" autoplay loop playsinline class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>


### Printed catalogue

- 165 × 235 mm, 168 pages
- 2000 copies
- CMYK + Pantone 876U

<video style="margin-top: 0;" src="/videos/video-book_chiragan.mp4" autoplay loop playsinline class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>


{{< img src="images/book-chiragan-1.jpg" >}}

{{< img src="images/chemin-fer_part2.png" class="border" >}}

{{< img src="images/print-inspector-1.png" class="border" >}}
{{< img src="images/print-inspector-2.png" class="border" >}}


{{< img src="images/book-chiragan-3.jpg" >}}
{{< img src="images/book-chiragan-4.jpg" >}}
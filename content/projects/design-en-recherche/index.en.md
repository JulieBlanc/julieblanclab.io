---
title: "Design en Recherche"
date: 2020-06-23T19:12:28+02:00
draft: false
publish: true
id: "design-en-recherche"
---

The association "Design en recherche", formed in March 2013, is a network of doctoral students and young PhDs whose research focuses on emerging themes of study and practice of design. In 2020, we worked on a website redesign with the members of the association. I did the design and development of the website. <br>
The website was developed with the Hugo static site generator, the source code is on Github and was deployed with Netlify. In order for members to easily edit the content from a graphical interface, I also plugged the site into Forestry.io.


- Website: [designenrecherche.org](https://designenrecherche.org/)
- Developed in collaboration with the members of the association 
- June 2020 - October 2020
- Source code on [GitHub](https://github.com/designenrecherche/site-hugo)
  
  
{{< img src="images/repro_design-en-recherche.png" >}}


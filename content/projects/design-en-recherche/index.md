---
title: "Design en Recherche"
date: 2020-06-23T19:12:28+02:00
draft: false
publish: true
id: "design-en-recherche"
---

L’association «&#8239;Design en recherche&#8239;», formée en mars 2013, est un réseau de doctorant·e·s et jeunes docteur·e·s dont les recherches portent sur les thématiques émergentes d’étude et de pratique du design. En 2020, nous avons travaillé à une refonte du site avec les membres de l'assoiation. J'ai réalisé la conception et le développement du site web. <br>
Le site a été développé avec le générateur de site statique Hugo, le code source est sur Github et a été déployé avec Netlify. Afin que les membres puissent éditer facilement le contenu depuis une interface graphique, j'ai branché le site sur Forestry.io.



- Site web: [designenrecherche.org](https://designenrecherche.org/)
- Développé en collaboration avec les membres de l'association 
- Juin 2020 - octobre 2020
- Code source sur [GitHub](https://github.com/designenrecherche/site-hugo)


{{< img src="images/repro_design-en-recherche.png"  >}}


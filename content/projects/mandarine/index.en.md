---
title: "Mandarine"
date: 2019-09-15T19:14:07+02:00
draft: false
publish: true
id: "mandarine"
---

Mandarine magazine aims to explore and reveal connections between the fields of architecture, design and contemporary art by focusing on their research topics, practices, and methodologies. The project is based on a multi-media distribution that can take four forms: print magazine, website, personal printing (office printer) and ePub digital publication.

- Project started in 2017 in collaboration with Dimitri Charrel
- Project abandoned due to lack of funding in 2019


{{< img src="images/mandarine_01.jpg" >}}
{{< img src="images/mandarine_02.png" >}}
{{< img src="images/mandarine_03.jpg" class="portrait">}}
{{< img src="images/mandarine_04.png" >}}
{{< img src="images/mandarine_05.jpg" >}}
{{< img src="images/mandarine_06.jpg" >}}
{{< img src="images/mandarine_07.png" >}}
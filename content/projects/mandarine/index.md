---
title: "Mandarine"
date: 2019-09-15T19:12:28+02:00
draft: false
publish: true
id: "mandarine"
---

La revue Mandarine a pour objectif d’explorer et révéler les connexions entre les champs de l’architecture, des designs et de l’art contemporain en s’intéressant à leurs sujets de recherches, leurs pratiques, et leurs méthodologies. Le projet repose sur une diffusion multisupport pouvant prendre quatre formes : revue imprimée, site web, impression personnelle (bureautique) et publication numérique ePub.

- Développé en collaboration avec Dimitri Charrel en 2017
- Abandon du projet pour cause de manque de financement en 2019




{{< img src="images/mandarine_01.jpg" >}}
{{< img src="images/mandarine_02.png" >}}
{{< img src="images/mandarine_03.jpg" class="portrait">}}
{{< img src="images/mandarine_04.png" >}}
{{< img src="images/mandarine_05.jpg" >}}
{{< img src="images/mandarine_06.jpg" >}}
{{< img src="images/mandarine_07.png" >}}
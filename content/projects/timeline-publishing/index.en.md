---
title: "Timeline of technologies for publishing"
date: 2019-09-15T19:20:07+02:00
draft: false
publish: true
id: "timeline-publishing"
aliases:
    - /projets/timeline-publishing.html
---

This timeline represents the non-exhaustive technical history of digital publishing. It is divided into several layers: hardware, operating systems and platforms / reading softwares / publihsing softwares / softwares and systems for text / formats and languages. The timeline was developed with HTML and CSS; the printed version measures 240cm × 60cm.

A short French version was published in the 8th issue of *Sciences du Design*. The digital version is available below.

- Licence Creative Commons CC-BY
- Source code avaible on [gitlab](https://gitlab.com/JulieBlanc/timeline-publishing)
- Published in *Sciences du Design*, [Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm)

{{< img src="images/20190209-timeline-punlishing.png">}}

{{< iframe src="http://recherche.julie-blanc.fr/timeline-publishing/" >}}

<span class="link-iframe"><a href="http://recherche.julie-blanc.fr/timeline-publishing/">See full screen version</a></span>


“From the earliest character encoding computing standards (ASCII 1963) to the recent ISO document format standards (PDF 2008, EPUB 3 2014, OpenDocumen 2016), digital text has  been an integral part of the history of technology. Without going into the conceptual models that went with or led to these, this non exaustive historical timeline seeks to cover everything associated with the design, production, and dissemination of digital editions : frameworks, platforms, software, tools, formats, legislation, guidelines, and standards. Two worlds long thought to be quite foreign to one another, namely the worlds of print (fixed page layout paradigm resulting from a sequential process) and screen (iterative web model constantly updated and adjusted according to the context in which it is read), appear to converge. The convergence occurs when web technology is adapted to print, as illustrated in this graphic document : this historical timeline has itself been created in  HTML and CSS.”

This work has benefited from the contributions of Lucile Haute, Robin De Mourat, Antoine Fauchié, Anthony Masure, and Julien Taquet.
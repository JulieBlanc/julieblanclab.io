---
title: "Timeline des technologies de l'édition numérique"
date: 2019-09-15T19:17:21+02:00
draft: false
publish: true
id: "timeline-publishing"
aliases:
    - /projets/timeline-publishing.html
---

Cette timeline représente l'histoire technique non exhaustive de l'édition numérique. Elle se divise en plusieurs couches : hardware, systèmes d'exploitation et plateformes / logiciels de lecture / logiciels de PAO / logiciels et systèmes pour l'écriture / formats et langages. Elle a été développée en HTML et CSS et la version imprimée mesure 240cm × 60cm.

Une version raccourcie française a été publiée dans le 8e numéro de *Sciences du Design*. La version numérique est diponible ci-dessous.

- Licence Creative Commons CC-BY
- Code source sur [gitlab](https://gitlab.com/JulieBlanc/timeline-publishing)
- Publié dans *Sciences du Design*, [Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm)

{{< img src="images/20190209-timeline-punlishing.png">}}


{{< iframe src="http://recherche.julie-blanc.fr/timeline-publishing/" >}}

<span class="link-iframe"><a href="http://recherche.julie-blanc.fr/timeline-publishing/">Aller voir la version plein écran</a></span>

« Depuis les premières normes informatiques de codage de caractère (ASCII, 1963) jusqu’aux récentes inscriptions de formats de documents (PDF 2008, EPUB 3  2014, OpenDocument 2016) à  l’ISO, les rapports du texte au numérique sont indissociables d’une histoire technique. Sans aborder les modèles conceptuels qui ont précédé ou accompagné ces inventions, cette frise chronologique non exhaustive entend recouvrir l’ensemble des supports, plateformes, logiciels, outils, formats, législations, standards et normes appliqués à la conception, la production et la diffusion d’éditions numériques. Il apparaît que des mondes longtemps pensés comme étrangers l’un à l’autre, à savoir : l’imprimé (paradigme de la mise en page fixe et résultant d’un processus séquentiel) et l’écran (modèle du web itératif sans cesse actualisé et interprété en fonction du contexte de lecture), se rejoignent. La jonction opère au moment où des technologies du web sont adaptées à l’imprimé ainsi que ce document graphique l’illustre : cette frise chronologique a été réalisée en HTML et CSS. »
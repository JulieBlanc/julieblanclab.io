---
title: "Le cahier de l'erg"
id: "cahier-erg"
date: 2019-09-14T22:48:08+02:00
draft: false
publish: true
aliases:
    - /projets/cahier-erg.html
---

Realization of the presentation brochure of the erg (*école de recherche graphique*), the Brussels-based Higher School of Arts, which bases its teaching on multidisciplinarity.

- Client: erg, école de recherche graphique
- With Cyriel Makhoul et Florence Grosse
- 1000 copies
- 160 × 240 mm, 32 pages
- Aged internal poster 420 × 594 mm
- Print: Hayez, Bruxelles

{{< img src="images/cahier-erg-01.jpg" >}}
{{< img src="images/cahier-erg-02.jpg" class="portrait border" >}}
{{< img src="images/cahier-erg-03.jpg" >}}
{{< img src="images/cahier-erg-04.jpg" >}}
{{< img src="images/cahier-erg-05.jpg" >}}
{{< img src="images/cahier-erg-06.jpg" >}}
{{< img src="images/cahier-erg-07.jpg" >}}
---
title: "Le cahier de l'erg"
id: "cahier-erg"
date: 2019-09-14T22:48:08+02:00
draft: false
publish: true
aliases:
    - /projets/cahier-erg.html
---

Réalisation de la brochure de présentation de l’erg (école de recherche graphique), école supérieure des arts située à Bruxelles qui base son enseignement sur la pluridisciplinarité.

- Client : erg, école de recherche graphique
- Réalisé avec Cyriel Makhoul et Florence Grosse
- 1000 exemplaires
- 160 × 240 mm, 32 pages
- Poster intérieur agraphé 420 × 594 mm
- Impression : Hayez, Bruxelles

{{< img src="images/cahier-erg-01.jpg" >}}
{{< img src="images/cahier-erg-02.jpg" class="portrait border" >}}
{{< img src="images/cahier-erg-03.jpg" >}}
{{< img src="images/cahier-erg-04.jpg" >}}
{{< img src="images/cahier-erg-05.jpg" >}}
{{< img src="images/cahier-erg-06.jpg" >}}
{{< img src="images/cahier-erg-07.jpg" >}}
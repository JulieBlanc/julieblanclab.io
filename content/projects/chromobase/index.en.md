---
title: "Chromobase"
date: 2025-03-05T19:12:28+02:00
draft: false
publish: true
id: "chromobase"
---



The Chromobase is the open access database of the ERC project [Chromotope](https://chromotope.eu/). This EU-funded project (2019-2025) explores what happened to colour across industrial Europe (France, Britain and Germany) in the second half of the 19th century.<br>
It is a hybrid digital object, both a collection of scholarly articles and a database. The project is based on a narrative approach to data, with a series of articles enhanced by hypertextual materials (people, organizations, objects, techniques, colour names, events).


- Website: [chromobase.huma-num.fr](https://chromobase.huma-num.fr/)
- October - December 2023
- Design and development of HTML and CSS templates
- CMS and database development by [OuestWare](https://www.ouestware.com/)


<video style="margin-top: 0;" src="/videos/chromobase-screenshot.mp4" autoplay loop playsinline class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>
 
We opted for a homepage that immediately showcases the different facets of exploration within the publication: hypertextual articles, a timeline of these articles, various groups of notices available (people, organizations, objects, techniques, colour names, events), and a colour wheel.



{{< img-assets src="chromobase/chromobase-home.png" alt="Homepage of Chromobase website" class="border" caption="Homepage of Chromobase website" >}}


Narrative pages are short scholarly articles, each presenting a specific case or aspect of the project. Hyperlinks reference the various notices in the publication (people, organizations, objects, techniques, colour names, events) as well as bibliographical references. These narratives help contextualize the project's data and highlight their interactions.

{{< img-assets src="chromobase/chromobase-narratives-1.png" alt="Example of a narrative page" class="border">}}


{{< img-assets src="chromobase/chromobase-narratives-2.png" alt="Example of a narrative page" class="border" caption="Example of two narrative pages" >}}

Notices pages (person, event, object, technique, colour, or reference) are both informative, providing additional details, and exploratory, as they always refer back to the narratives that cite them, much like a glossary. Users can also browse these notices through list pages featuring filterable and sortable lists of items by type.

{{< img-assets src="chromobase/chromobase-people.png" alt="A notice page in the ‘People’ category with related articles" class="border" caption="A notice page in the ‘People’ category with related articles" >}}

{{< img-assets src="chromobase/chromobase-people-list.png" alt="List of all notices in the ‘People’ category, with sorting options" class="border" caption="List of all notices in the ‘People’ category, with sorting options" >}}


Numerous metadata attributes have been associated with "Objects" notices, some of which have been digitized in high definition specifically for this project.

{{< img-assets src="chromobase/chromobase-objects.png" alt="A page in the ‘Objects’ category with images, various metadata and associated articles" class="border" caption="A page in the ‘Objects’ category with images, various metadata and associated articles" >}}

{{< img-assets src="chromobase/chromobase-objects-list.png" alt="List of all notices in the ‘Objects’ category, with sorting options" class="border" caption="List of all notices in the ‘Objects’ category, with sorting options" >}}

The colour wheel is based on the first colour systems including brightness and chroma published by the chemist Michel-Eugène Chevreul (1786-1889) in *Colours and Their Application to Industrial Arts Using Chromatic Circles* (1864). We used the HSV space as it is the contemporary colour space closest to his "chromatic circles." Users can explore publication objects arranged in different layers according to their dominant colours.

{{< img-assets src="chromobase/chromobase-color-wheel-2.png" alt="Page with colour wheel and selected object" class="border" caption="Page with colour wheel and selected object" >}}

This project has been fueled by an interdisciplinary dynamic combining humanities, graphic design, web development and structured publishing. The close collaboration between the research team, OuestWare, and me aimed to bring together epistemology and materiality, where technical and visual devices are developed in resonance with research issues.

The technical part was carried out by [OuestWare](https://www.ouestware.com/), which developed a custom text/database editor built using the headless Content Management System ([Keystone.js](https://keystonejs.com/)) and designed to consume as few resources as possible.

My role was to design the site's ergonomics and interface to facilitate navigation through these datasets. I also provided the HTML templates and CSS stylesheets.

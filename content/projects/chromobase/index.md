---
title: "Chromobase"
date: 2025-03-05T19:12:28+02:00
draft: false
publish: true
id: "chromobase"
---



Chromobase est une base de données en libre accès développée dans le cadre du projet ERC&nbsp;[Chromotope](https://chromotope.eu/), une exploration de ce qu'il est advenu de la couleur à travers l'Europe industrielle (France, Grande-Bretagne et Allemagne) dans la seconde moitié du XIXe siècle.<br> 
C’est un objet numérique hybride, à la fois ensemble d'articles scientifiques et base de données.  Le projet est basé sur une approche narrative des données, avec une série d’articles augmentés de matériaux hypertextuels (people, organisations, objects, techniques, colour names, event). 


- Site web: [chromobase.huma-num.fr](https://chromobase.huma-num.fr/)
- Octobre - décembre 2023
- Design et développement des templates HTML et CSS
- Développement du CMS et de la base de donnée par [OuestWare](https://www.ouestware.com/) 


<video style="margin-top: 0;" src="/videos/chromobase-screenshot.mp4" autoplay loop playsinline class="border-video">
  Votre navigateur ne gère pas l'élément <code>video</code>.
</video>
 


Nous avons fait le choix d’une page d’accueil montrant d’emblée les différentes facettes d’exploration de la publication: les articles hypertextuels, une frise chronologique de ces mêmes articles, les différents groupes de notices disponibles (people, organisations, objects, techniques, colour names, event) et une roue chromatique. 


{{< img-assets src="chromobase/chromobase-home.png" alt="Page d’acceuil du site web Chromobase" class="border" caption="Page d’acceuil du site web Chromobase" >}}


Les pages narratives sont de courts articles scientifiques présentant chacun un cas ou un aspect spécifique du projet. Les liens hypertextes font références aux différentes notices présentes dans la publication (people, organisations, objects, techniques, colour names, event) et aux références bibliographiques. Ces récits permettent donc de contextualiser les différentes données du projets et mettre en évidence leurs interactions.

{{< img-assets src="chromobase/chromobase-narratives-1.png" alt="Exemple d’une page  de type narrative" class="border">}}


{{< img-assets src="chromobase/chromobase-narratives-2.png" alt="Exemple d’une page  de type narrative" class="border" caption="Exemple de deux pages  de type narrative" >}}

Les pages de notices (people, organisations, objects, techniques, colour names, event) sont à la fois informatives, en précisant quelques informations supplémentaires, et exploratoires car elles renvoient toujours aux récits qui les citent, comme le ferait un glossaire. On peut également consulter ces notices grâce aux pages de listes qui proposent des listes filtrables et triables d'éléments par type.

{{< img-assets src="chromobase/chromobase-people.png" alt="Une page de type notice" class="border" caption="Une page de type notice, catégorie « People » avec les articles associés" >}}

{{< img-assets src="chromobase/chromobase-people-list.png" alt="Liste des notices people" class="border" caption="Liste de toutes les notices de la catégories « People », avec possibilités de&nbsp;tri" >}}



De nombreuses métadonnées ont été associées aux notices «Objects», dont certains ont été spécialement numérisé pour ce projet en haute définition.

{{< img-assets src="chromobase/chromobase-objects.png" alt="Une page de type Objects" class="border" caption="Une page de type notice, catégorie « Objects » avec images, métadonnées diverses et articles associés" >}}

{{< img-assets src="chromobase/chromobase-objects-list.png" alt="Liste des objects, avec image" class="border" caption="Liste de toutes les notices de la catégories « Objects », avec possibilités de&nbsp;tri" >}}

La roue chromatique fait référence aux premiers systèmes de couleurs publiés par le chimiste Michel-Eugène Chevreul dans [*Des couleurs et leurs applications aux arts industriels à l'aide des cercles chromatiques*](https://gallica.bnf.fr/ark:/12148/bpt6k1510069n.texteImage) (1864). Nous avons utilisé l'espace HSV car c'est l'espace contemporain le plus proche de ses « cercles chromatiques ».  Il est possible d’explorer les objets de la publication placés sur différentes couches en fonction de leurs couleurs principales.

{{< img-assets src="chromobase/chromobase-color-wheel-2.png" alt="Page avec roue chromatique" class="border" caption="Page avec roue chromatique et object sélectionné" >}}

Ce projet a été nourri par une dynamique interdisciplinaire alliant sciences humaines, design graphique, développement web et édition structurée. La collaboration étroite entre l'équipe de recherche, OuestWare, et moi-même a eu pour objectif de faire se rencontrer épistémologie et matérialité, où des dispositifs techniques et visuels sont développés en résonance avec les problématiques de la recherche.

La partie technique a été réalisée par Ouest Ware qui a proposé un éditeur de texte/base de données personnalisé construit à l'aide du système de gestion de contenu statique [Keystone.js](https://keystonejs.com/), et conçu de tel manière à consommer le moins de ressources possibles.

 Mon rôle a été de concevoir l’ergonomie et le design du site afin de faciliter la navigation entre ces données. J’ai aussi fourni les templates HTML et les feuilles de styles CSS.


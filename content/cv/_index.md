---
title: "Curriculum vitae"
draft: false
---


{{% div class="header-cv cv-part" %}}

Julie Blanc \
née le 23/03/1992 \
<span class="website"><a href="https://julie-blanc.fr/">julie-blanc.fr</a><br></span>
\
15 rue Georges Pitard \
75015 Paris (France) \
+33 (0)6 31 51 36 55 \
<span class="mail">contact@julie-blanc.fr</span>
{{% /div %}}


{{% div class="intro-cv cv-part" %}}


- Design graphique indépendante <span class="date">depuis 2016</span>
- Conception et développement de Paged.js (pagedjs.org)
- Docteure en ergonomie et design graphique (Université Paris / EnsadLab / EUR Artec)

{{% /div %}}

{{% div class="formation cv-part" %}}

# Formation 

## 2023

- **Doctorat en ergonomie et design graphique**
Laboratoire Paragraphe, Université Paris 8 et EnsadLab-Paris dans le cadre de l’école universitaire de&nbsp;recherche ArTeC (Art, Technologies et Création)

## 2018 


- **Pré-doctorat EnsadLab** \
École nationale supérieure des Arts Décoratifs, Paris (France)

## 2016 

- **Master erg.edit : politique du multiple** \
Option typographie et arts numériques \
école de recherche graphique (erg), Bruxelles (Belgique)

## 2014

- **Licence de concepteur designer** \
Axe Sud, Toulouse (France)


## 2010

- **Baccalauréat Scientifique** \
Option physique-chimie \
Lycée Bellevue, Toulouse

{{% /div %}}





{{% div class="associatif cv-part" %}}

# Associatif et collectifs

## Les rencontres de Lure (comité depuis 2019)
L’association, depuis plus de 60 ans, examine l’écriture, son évolution et ses formes (typographique, littéraire, visuelle ou numérique) lors d'une semaine de conférences et rencontres.



## PrePostPrint (participation informelle)
Groupe de personnes qui cultive un intérêt pour les procédés de création graphique et&nbsp;les&nbsp;systèmes de publication libres considérés comme « alternatifs » ou « non conventionnels », particulièrement s’ils sont conçus avec les technologies du web.

## Design en recherche (présidente en 2021)
Réseau de doctorant·e·s et jeunes docteur·e·s dont les recherches portent sur les thématiques émergentes d’étude et de pratique du design.



{{% /div %}}


{{% div class="conferences cv-part" %}}

# Conférences <small>(sélection)</small>

## 2021 
- **Composer une recherche en design graphique et en ergonomie&#8239;: une palette d'activités hétérogènes**,
<span class="print-only"><br></span>journée d'étude *Design graphique, manières de faire de la recherche*, Centre Pompidou (Paris, France), <span class="date">17&nbsp;décembre&nbsp;2021<span>.
- **Visibilité du schème et hybridations instrumentales, l'introduction des technologie du web dans l'activité de composition paginée des designers graphiques**, <span class="print-only"><br></span>colloque *Approche instrumentale, expansion et devenirs*, Université Paris 8 (France), <span class="date">26 novembre 2021<span>.
- **Intervenir au cœur des institutions&#8239;: tactiques et problématiques de designer-chercheur·euse·s**&nbsp;[fr], *Pratiques de la recherche en design* #4 [en ligne], <span class="date">2 juin 2021<span>.
- **@media print { book }**&nbsp;[fr], Ésad Valence (France), <span class="date">8 mai 2021<span>.

## 2020
- **Web-binders: layout of two printed and digital publications**&nbsp;[en], Julie Blanc et&nbsp;Nicolas Taffin, *Open Publishing Fest* [en ligne], <span class="date">27 mai 2020<span>.
- **Présentation de paged.js pour le W3C Print CSS Workshop**&nbsp;[en], <span class="print-only"><br></span>*XML Prague* (République tchèque), <span class="date">13 février 2020</span>.
- **Présentation de la publication numérique et imprimée du catalogue <span class="print-only"><br></span>des sculptures de la villa romaine de Chiragan**&nbsp;[fr], Julie Blanc et Christelle Molinié, <span class="print-only"><br></span>*Les Lundis numériques de l'INHA* (Paris, France), <span dclass="date">13 janvier 2020</span>.

## 2019 

- **Du fanzine à l’encyclopédie, éditions avec et après le numérique**&nbsp;[fr], séminaire <span class="print-only"><br></span>*Dé-brancher l’œuvre*, master ArTeC, Université Paris&nbsp;8 (France), <span dclass="date">4 octobre 2019</span>.
- **Print Books with Browsers**&nbsp;[en], *Digital Publishing Summit*, BnF (Paris, France), <span class="date">26 mai 2019</span>.
- **Faire des livres avec un navigateur**&nbsp;[fr], Julie Blanc et Julien Taquet, *Journées&nbsp;du&nbsp;Logiciel Libre*, Maison pour tous (Lyon, France), <span dclass="date">6 avril 2019<span>.

## 2018 

- **Imprimé, single source publishing et technologies du web**&nbsp;[fr], colloque <span class="print-only"><br></span>*Repenser les&nbsp;humanités numériques*, CRIHN (Montréal, Canada),  <span dclass="date">27&nbsp;octobre 2018</span>.
- **Paginer le flux** [fr], présentation aux *Rencontres internationale de Lure* <span class="print-only"><br></span>(Lurs, France), <span class="date">20 août 2018</span>.
- **Design des publications scientifiques multisupports**&nbsp;[fr], Julie Blanc et Lucile Haute, colloque international *ÉCRiDiL, Écrire, éditer, lire à l’ère numérique*, Usine&nbsp;C <span class="print-only"><br></span>(Montréal, Canada),  <span dclass="date">3 avril 2018</span>.
- **PagedMedia approaches**&nbsp;[en], *Paged Media Meeting*, MIT&nbsp;press <span class="print-only"><br></span>(Cambridge, États-Unis), <span class="date">9 janvier 2018</span>.
- **Standards du web et publication académique**&nbsp;[fr], Julie Blanc et Lucile Haute, *PrePostPrint @ Gaîté Lyrique* (Paris, France), <span dclass="date">21 octobre 2018</span>.

## 2017

- **(Re)Penser les chaînes de publication&#8239;: soutenabilité et émancipation**&nbsp;[fr], <span class="print-only"><br></span>Julie blanc et Antoine Fauchié, colloque international *Les écologies du numérique*, ÉSAD Orléans – Écolab (Orléans, France),  <span class="date">10 novembre 2017</span>.

{{% /div %}}


{{% div class="publications cv-part" %}}

# Publications

- Julie Blanc, «&#8239;Composer une recherche en design graphique et en ergonomie&#8239;: une palette d’activités hétérogènes&#8239;», *Design graphique, manière de faire de la recherche*, à paraître.
- Antoine Fauchié et Julie Blanc, «&#8239;(Re)Penser les chaînes éditoriales&#8239;: soutenabilité et émancipation&#8239;», actes de la journée d'étude *Les écologies du numérique* (10 novembre 2017), à paraître.
- Julie Blanc, «&#8239;La création en actes (compte rendu)&#8239;», *Azimuts*, n<sup>o</sup>52, «&#8239;Continu&#8239;», 2021, pp. 198-201.
- Julie Blanc et Lucile Haute, «&#8239;Technologies de l’édition numérique&#8239;», *Sciences du design*, 2018/2 (n<sup>o</sup>&nbsp;8), pp.&nbsp;11-17. URL: [www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm](https://www.cairn.info/revue-sciences-du-design-2018-2-page-11.htm)
- Lucile Haute et Julie Blanc, «&#8239;Publier la recherche en design&#8239;: (hors-)normes, (contre-)formats, (anti-)standards&#8239;», *Réel | Virtuel&#8239;: enjeux du numérique*, n<sup>o</sup> 6, «&#8239;Les normes du numériques&#8239;», 2018. URL: [www.reel-virtuel.com/numeros/numero6/sentinelles/publier-recherche-design](http://www.reel-virtuel.com/numeros/numero6/sentinelles/publier-recherche-design)
- Julie Blanc et Lucile Haute, «&#8239;Standards du web et publication académique&#8239;», *Code X – 01 : PrePostPrint*, éditions HYX, 2017.

{{% /div %}}


{{% div class="events cv-part" %}}


# Organisation d'évènements

## 2021 
- **Pratiques de la recherche en design**, cycle de conférence de l'association <span class="print-only"><br></span>Design en Recherche, 2021.
- **Manières de voir, manières de penser le monde. Méthodologie(s) à l’épreuve du réel et pratiques (pluri-)disciplinaires**, colloque CLI 2021, Université Paris 8 [en ligne],  <span class="date">4 mars 2021</span>. 

## 2018
- **EnsadLab invite PrePostPrint&#8239;: Enjeux des systèmes de publication libres et outils alternatifs pour la création graphique**, colloque, EnsAD (Paris),  <span class="date">3–4 avril 2018</span>. 
- **Éditer une revue “arts et sciences” aujourd’hui**, table ronde à l'évènement <span class="print-only"><br></span>*Nous ne sommes pas le nombre que nous croyons être*, Cité des arts (Paris, France), <span class="date">2 février 2018</span>.

## 2017
- **PrePostPrint @ Gaîté Lyrique**, conférences et salon de l’édition, Gaîté Lyrique (Paris), <span class="date">20–21 octobre 2017</span>.

{{% /div %}}


{{% div class="enseignement cv-part" %}}

# Enseignement, workshop et jury

## 2021
- **Workshop Paged.js**, Ésac Cambrai,27-29 octobre 2021.
- **Jury DNSEP option Design des média**, Ésad Orléans, <span class="date">23-24 juin 2021<span>.
- **Workshop « @media print { book } »**,  Ésad Valence, <span class="date">10-12 mai 2021<span>.
- **Workshop Paged.js**,  DNMADE mention design graphique, <span class="print-only"><br></span>Lycée André Malraux (Montereau-Fault-Yonne), <span class="date">16-18 mars 2021<span>.


## 2020
- **Cours d'introduction à l'ergonomie**, Licence 1, UFR Psychologie, Paris 8 (1 semestre).
- **Module de recherche Création «&#8239;Les formes du web&#8239;»**, <span class="print-only"><br></span>École nationale supérieure des&nbsp;Arts Décoratifs <span class="date">(1 semestre)<span>.
    - [workshops.julie-blanc.fr/2020-mrc-ensad](http://workshops.julie-blanc.fr/2020-mrc-ensad/)
- **Grand atelier «&#8239;Scripter Gutenberg&#8239;: des publication de papier et d'écran&#8239;»**, ÉSAD&nbsp;Orléans, <span class="date">21-27 janvier 2020<span>.
    - [workshops.julie-blanc.fr/2020-esad-orleans](http://workshops.julie-blanc.fr/2020-esad-orleans/) 
- **Workshop Paged.js**, DNMADE graphisme augmenté, <span class="print-only"><br></span>Lycée Jacques Prévert (Boulogne-Billancourt), <span class="date">24-25 novembre 2020<span>.

## 2019
- **Module de recherche Création «&#8239;Libre Hybridation&#8239;»**, <span class="print-only"><br></span>École nationale supérieure des Arts Décoratifs <span class="date">(1 semestre)<span>.
    - [workshops.julie-blanc.fr/libre-hybridation-2019](http://workshops.julie-blanc.fr/libre-hybridation-2019/) 

## 2018
- **Module de recherche Création «&#8239;Libre Hybridation&#8239;»**, <span class="print-only"><br></span>École nationale supérieure des Arts Décoratifs <span class="date">(1 semestre)<span>.

{{% /div %}}


{{% div class="workshops cv-part" %}}

# Workshops hors enseignement

## 2021
- **Paged.js hackathon**, <span class="print-only"><br></span>École nationale supérieure des Arts Décoratifs (France), <span class="date">1er-2 mars 2021<span>.
    - [julie-blanc.fr/blog/2021-04-16_hackathon-pagedjs](https://julie-blanc.fr/blog/2021-04-16_hackathon-pagedjs/)

## 2019
- **Atelier «&#8239;Markdown et Bibliographie&#8239;»**, <span class="print-only"><br></span>pour Design en Recherche, CRI Paris (France), <span class="date">18 juin 2019<span>.
- **Workshop «&#8239;Création hybride avec des outils numériques libres&#8239;»**, <span class="print-only"><br></span>*Les écrits du&nbsp;numérique #4*, Alphabetville et la Marelle, Friche La Belle de Mai, <span class="print-only"><br></span>Marseille (France), <span class="date">23 mars 2019<span>.
- **Workshop «&#8239;Paged.js: use CSS to design books&#8239;»**, <span class="print-only"><br></span>*Libre Graphics Meeting*, Saarbrücken (Allemagne), <span class="date">1er juin 2019<span>.

## 2018
- **Workshops «&#8239;Paged Media × PrePostPrint&#8239;»**, <span class="print-only"><br></span>Open Source Publishing lab, Bruxelles (Belgique) et École nationale supérieure des&nbsp;Arts Décoratifs, Paris (France), <span class="date">28-30 novembre 2018</span>.
    - [http://urlr.me/5TFLp](https://web.archive.org/web/20190506055446/https://www.pagedmedia.org/an-overview-of-the-paged-media-x-prepostprint-workshop/)
- **Atelier «&#8239;Écrire et  mettre en forme&#8239;: quels formats, quels outils&#8239;?&#8239;»** <span class="print-only"><br></span>pour Design en Recherche, EnsAD, Paris (France), <span class="date">5 juillet 2018</span>.

{{% /div %}}


{{% div class="projets cv-part" %}}

# Projets <small>(sélection)</small>

## Paged.js
Librairie JavaScript open-source pour permettre l’export de PDF et l’affichage paginé dans le(s)&nbsp;navigateur(s) à partir d’HTML et CSS. 
- [pagedjs.org](https://www.pagedjs.org/)

## «&#8239;Les sculptures de la villa romaine de Chiragan&#8239;», musée Saint-Raymond
Direction artistique, conception et développement d'un catalogue multisupport web et imprimé; accompagné de la conception d'une chaîne de publication modulaire open-source et collaborative spécifique (*single source publishing*).
- [julie-blanc.fr/projects/villa-chiragan](https://julie-blanc.fr/projects/villa-chiragan/)

## Site du médialab de SciencesPo
Direction artistique, conception et développement du site web du médialab de SciencesPo selon&nbsp;une approche de «&#8239;décroissance heureuse&#8239;» (no image / no fonts / no JavaScript).
- [julie-blanc.fr/projects/medialab](https://julie-blanc.fr/projects/medialab/)

## «&#8239;Héroïnes. Repenser les héros dans les œuvres et les pratiques artistiques&#8239;» 
Direction artistique, conception et développement d'une publication web multimédia <span class="print-only"><br></span>pour le&nbsp;groupe de recherche de la Fémis&#8239;; mise en place d'un *backend* spécifique.
- [repenserlesheros.femis.fr](https://repenserlesheros.femis.fr/)

## Site de l'association Design en Recherche
Direction artistique, conception et développement du site web de l'association Design en&nbsp;Recherche&#8239;;  mise en place d'un *backend* spécifique.
- [julie-blanc.fr/projects/design-en-recherche](https://julie-blanc.fr/projects/design-en-recherche/)


## Timeline des technologies de l’édition numérique
Recherche, édition et conception d'une timeline présentant l’histoire technique <span class="print-only"><br></span>non exhaustive de&nbsp;l’édition numérique. 
- [julie-blanc.fr/projects/timeline-publishing](https://julie-blanc.fr/projects/timeline-publishing/)


{{% /div %}}



document.addEventListener('DOMContentLoaded', (event) => {
    
    document.querySelector("#header").classList.add("screen-only");
    document.querySelector("#footer").classList.add("screen-only");
    document.querySelector("#header-socials").classList.add("screen-only");

    document.querySelector("#button-print").addEventListener("click", printfunc);
    
});

function printfunc(){
    let pageCV = document.querySelector("#page-cv");

    if(pageCV){
        let flowBook = document.querySelector("#cv");
        
        let bookContent = flowBook.innerHTML;
        let paged = new Paged.Previewer();
        paged.preview(bookContent, ["/scss/main.min.css"], document.querySelector("#printcv")).then((flow) => {
        });

        flowBook.remove();
        document.querySelector("#button-print").remove();
    }
   
}


class endRender extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    afterRendered(pages){
        
        window.print();
    }
}
Paged.registerHandlers(endRender);